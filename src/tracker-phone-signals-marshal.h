
#ifndef __TRACKER_PHONE__MARSHAL_H__
#define __TRACKER_PHONE__MARSHAL_H__

#include	<glib-object.h>

G_BEGIN_DECLS

/* VOID:STRING,STRING,STRING */
extern void TRACKER_PHONE__VOID__STRING_STRING_STRING (GClosure     *closure,
                                                       GValue       *return_value,
                                                       guint         n_param_values,
                                                       const GValue *param_values,
                                                       gpointer      invocation_hint,
                                                       gpointer      marshal_data);

G_END_DECLS

#endif /* __TRACKER_PHONE__MARSHAL_H__ */

