/*
 *  Paperbox - main-window.hh
 *
 *  Copyright (C) 2007-2008 Marko Anastasov
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#ifndef __PAPERBOX_MAIN_WINDOW__
#define __PAPERBOX_MAIN_WINDOW__

#include <tr1/memory>
#include <gtkmm/box.h>
#include <gtkmm/paned.h>
#include <gtkmm/window.h>
#include <gtk/gtkwindow.h>
#include <libglademm.h>
#include <gtkmm-utils/tile.h>
#include "category-view.hh"
#include "document-tile-view.hh"
#include "tag-cloud-model.hh"
#include "tag-cloud.hh"
#include "tile-set.hh"
#include "thumbnailer.hh"

namespace paperbox {

    class Browser;
    class CategoryModel;
    class Config;
    class Document;

    // These consts are used by configuration code.
    const int DEFAULT_WIDTH = 800;
    const int DEFAULT_HEIGHT = 690;
    const int DEFAULT_CATEGORY_PANED_POS = 280;

    class MainWindow : public Gtk::Window
    {
    public:
        explicit MainWindow(GtkWindow* cobject,
                            const Glib::RefPtr<Gnome::Glade::Xml>& glade);
        virtual ~MainWindow();

        static MainWindow* create();

    protected:
        void init_gui(const std::tr1::shared_ptr<Config>& cfg);

        void get_widgets_from_ui_file();
        void init_toolbar();
        void init_sorting_area();
        void setup_tiles();
        
        void connect_signals();

        void setup_categories();
        void reload_category_view();

        void render_new_tile_set(
            const std::vector<std::tr1::shared_ptr<Document> >& docs);

        void render_documents(
            const std::vector<std::tr1::shared_ptr<Document> > docs);

        /*** Signal handlers ***/
        void on_menu_document_open();
        void on_menu_document_tag();
        void on_menu_document_properties();
        void on_menu_document_quit();
        void on_menu_view_first_page();
        void on_menu_view_previous_page();
        void on_menu_view_next_page();
        void on_menu_view_last_page();
        void on_menu_about();
        
        void on_retrieval_started();

        void on_retrieval_finished();

        void on_new_blank_documents(const std::vector<std::tr1::shared_ptr<Document> >& docs);

        void on_document_retrieved(const std::tr1::shared_ptr<Document>& doc);

        void on_document_tile_selected(/*Document*/Gtk::Util::Tile& t);

        // Handler for DocumentTileView's and TagCloud's signals
        void on_tag_clicked(const Glib::ustring& tag);

        void on_edit_category();

        bool on_category_selected(const Glib::RefPtr<Gtk::TreeModel>& ,
                                  const Gtk::TreeModel::Path& path,
                                  bool path_currently_selected);

        void on_sorting_changed();

        // some widgets should not be available while retrieval gives
        // stochastic ordering of documents
        void hide_widgets_in_retrieval();
        void show_widgets_after_retrieval();

        void sort_documents(std::vector<std::tr1::shared_ptr<Document> >& docs);

        /*** Members ***/
        Browser* browser_;

        Glib::RefPtr<Gnome::Glade::Xml> glade_;
        Glib::RefPtr<Gtk::UIManager> ui_manager_;
        Glib::RefPtr<Gtk::ActionGroup> action_group_;

        bool in_retrieval_; // flags retrieval from tracker being in progress

        /*@* Child widgets *@*/

        Gtk::VBox* menu_vbox_;

        // ~left side~
        Gtk::HPaned*      hpane_;
        Gtk::VBox*        left_top_vbox_;
        Gtk::HBox         sorting_hbox_;
        Gtk::Label        sorting_label_;
        Gtk::ComboBoxText sorting_combo_;
        DocumentTileView* tile_view_;

        std::tr1::shared_ptr<TileSet> tiles_;

        // ~right side~
        Gtk::VBox*   right_top_vbox_;
        Gtk::VPaned* right_vpane_;

        // categories

        Gtk::VBox* category_vbox_;
        std::tr1::shared_ptr<CategoryView> category_view_;
        Glib::ustring selected_cat_name_;

        Gtk::HBox*  category_buttons_hbox_;

        std::tr1::shared_ptr<CategoryModel> category_model_;

        // tag cloud
        Gtk::VBox*  tag_cloud_vbox_;
        Gtk::Label  label_tags_;

        std::tr1::shared_ptr<TagCloudModel> model_;
        TagCloud    tag_cloud_;
        Gtk::VBox   tag_box_;

        Thumbnailer thumbnailer_;

    private:
        // Non-copyable:
        MainWindow(const MainWindow& );
        MainWindow& operator=(const MainWindow& );
    };

} // namespace paperbox

#endif // __PAPER_BOXMAIN_WINDOW__
