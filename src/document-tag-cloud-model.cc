/*
 * Paperbox - document-tag-cloud-model.cc
 *
 * Copyright (C) 2007 Marko Anastasov
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "browser.hh"
#include "document.hh"
#include "document-tag-cloud-model.hh"

namespace paperbox {

    using std::vector;
    using std::tr1::shared_ptr;

    DocumentTagCloudModel::DocumentTagCloudModel(int min_font_size,
                                                 int max_font_size)
        :
        TagCloudModel(min_font_size, max_font_size)
    {
        Browser* b = Browser::instance();

        b->signal_tags_changed().connect(
            sigc::mem_fun(*this, &DocumentTagCloudModel::on_tags_changed));
    }

    DocumentTagCloudModel::~DocumentTagCloudModel()
    {
    }
    
    void
    DocumentTagCloudModel::on_tags_changed(
        const std::string& /* uri */,
        const vector<Glib::ustring>& tags_added,
        const vector<Glib::ustring>& tags_removed)
    {
        vector<Glib::ustring>::const_iterator it_add(tags_added.begin());
        vector<Glib::ustring>::const_iterator end_add(tags_added.end());

        for ( ; it_add != end_add; ++it_add)
            TagCloudModel::add_tag(*it_add);

        vector<Glib::ustring>::const_iterator it_rem(tags_removed.begin());
        vector<Glib::ustring>::const_iterator end_rem(tags_removed.end());

        for ( ; it_rem != end_rem; ++it_rem)
            TagCloudModel::remove_tag(*it_rem);
    }

    void
    DocumentTagCloudModel::update_tags(const shared_ptr<Document>& doc)
    {
        vector<Glib::ustring> tags = doc->get_tags();
        vector<Glib::ustring>::iterator it(tags.begin());
        vector<Glib::ustring>::iterator end(tags.end());

        for ( ; it != end; ++it) add_tag(*it);
    }

} // namespace paperbox
