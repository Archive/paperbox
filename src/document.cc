// -*- Mode: C++; indent-tabs-mode: nil; c-basic-offset: 4 -*-

/*
 *  PaperBox - document.cc
 *
 *  Copyright (C) 2007 Marko Anastasov
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include <cstdlib>
#include <algorithm>
#include <functional>
#include <iostream>
#include <vector>
#include <glibmm/date.h>
#include <glibmm-utils/ustring.h>
#include "document.hh"

namespace {

    // A simple functor for printing ostream-able types.
    template<class T>
    struct unary_print : public std::unary_function<T, void>
    {
        unary_print(std::ostream& out) : os(out), count(0) {}
        void operator() (T x) { os << x << ' '; ++count; }

        std::ostream& os;
        int count;
    };

} // anonymous namespace

namespace paperbox {

    using std::vector;

    Document::Document(const Glib::ustring& uri)
        :
        uri_(uri),
        page_count_(0),
        word_count_(0)
    {
        set_file_name(uri_); // just so it's not completely blank at first
    }

    Document::~Document()
    {
    }

    void
    Document::print() const
    {
        std::cout << "URI: " << uri_ << '\n'
                  << "File name: " << file_name_ << '\n'
                  << "Last modified: " << modtime_ << '\n'
                  << "Subject: " << subject_ << '\n'
                  << "Author: " << author_ << '\n'
                  << "Comments: " << comments_ << '\n'
                  << "Page count: " << page_count_ << '\n'
                  << "Word count: " << word_count_ << '\n'
                  << "Date created: " << date_created_ << std::endl;

        std::cout << "Tags: ";
        std::for_each(tags_.begin(),
                      tags_.end(),
                      unary_print<Glib::ustring>(std::cout));

        std::cout << '\n' << std::endl;
    }

    Glib::ustring
    Document::get_file_name() const
    {
        return file_name_;
    }

    void
    Document::set_file_name(const Glib::ustring& file_name)
    {
        file_name_ = file_name;
    }

    Glib::ustring
    Document::get_modification_time() const
    {
        return modtime_;
    }

    void
    Document::set_modification_time(const Glib::ustring& file_modtime)
    {
        modtime_ = file_modtime;
    }

    guint64
    Document::get_mtime() const
    {
        return mtime_;
    }

    void
    Document::set_mtime(guint64 mtime)
    {
        mtime_ = mtime;
    }

    Glib::ustring
    Document::get_subject() const
    {
        return subject_;
    }

    void
    Document::set_subject(const Glib::ustring& subject)
    {
        subject_ = subject;
    }

    Glib::ustring
    Document::get_author() const
    {
        return author_;
    }

    void
    Document::set_author(const Glib::ustring& author)
    {
        author_ = author;
    }

    Glib::ustring
    Document::get_comments() const
    {
        return comments_;
    }

    void
    Document::set_comments(const Glib::ustring& comments)
    {
        comments_ = comments;
    }

    int
    Document::get_page_count() const
    {
        return page_count_;
    }

    void
    Document::set_page_count(const Glib::ustring& page_count)
    {
        try {
            page_count_ = Glib::Util::convert_to<int>(page_count);
        } catch (const Glib::Util::BadConversion& ex) {
            LOG_EXCEPTION("could not convert page count string '"
                    << page_count << "' to an integer for document "
                    << uri_.raw());
            // In bugs like #552648, this would be the case of trackerd
            // returned page count string being '119 1', '8 1' etc.
            // So let's try to split by whitespace and try to convert
            // whatever is the first token.
            std::vector<Glib::ustring> tokens = Glib::Util::split(page_count);
            if (! tokens.size()) {
                page_count_ = 0;
                return;
            }

            try {
                page_count_ = Glib::Util::convert_to<int>(tokens[0]);
                LOG_DD("after a split, page count set to " << page_count_);
            } catch (const Glib::Util::BadConversion& ex) { // give up
                LOG_EXCEPTION("attempt after splitting by whitespace failed, "
                        << "setting to zero.");
                page_count_ = 0;
            }
        }
    }

    int
    Document::get_word_count() const
    {
        return word_count_;
    }

    void
    Document::set_word_count(const Glib::ustring& word_count)
    {
        try {
            word_count_ = Glib::Util::convert_to<int>(word_count);
        } catch (const Glib::Util::BadConversion& ex) {
            // no special handling here, as there have been no reports yet
            LOG_EXCEPTION("could not convert word count string '"
                    << word_count << "' to integer, setting to zero for "
                    << "document " << uri_.raw());
            word_count_ = 0;
        }
    }

    Glib::ustring
    Document::get_date_created() const
    {
        return date_created_;
    }

    void
    Document::set_date_created(const Glib::ustring& date_created)
    {
        date_created_ = date_created;
    }

    vector<Glib::ustring>
    Document::get_tags()
    {
        return tags_;
    }

    void
    Document::set_tags(vector<Glib::ustring>& new_tags)
    {
        for ( vector<Glib::ustring>::iterator it(new_tags.begin());
              it != new_tags.end();
              ++it) {
            tags_.push_back(*it);
        }
    }

    bool
    Document::contains_tag(const Glib::ustring& tag) const
    {
        return (find(tags_.begin(), tags_.end(), tag) != tags_.end());
    }

    void
    Document::add_tag(const Glib::ustring& tag)
    {
        if (! contains_tag(tag))
            tags_.push_back(tag);
    }

    void
    Document::remove_tag(const Glib::ustring& tag)
    {
        vector<Glib::ustring>::iterator it(
            find(tags_.begin(), tags_.end(), tag));

        if (it == tags_.end()) return;
        tags_.erase(it);
    }

} // namespace paperbox
