/*
 * Paperbox - tag-cloud-model.cc
 *
 * Copyright (C) 2007 Marko Anastasov
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <cmath>
#include "tag-cloud-model.hh"

using std::map;
using std::tr1::shared_ptr;

class CloudStats
{
public:
    CloudStats()
        : font_range(0), min_tag_count(0), max_tag_count(0),
          min_log(0), max_log(0), log_range(1) {}

    ~CloudStats() {}

    int font_range;

    int min_tag_count;
    int max_tag_count;

    double min_log;
    double max_log;
    double log_range;
};

typedef struct TagData TagData;

struct TagData
{
    int count;
    int font_size;
};

typedef tag_map::iterator tag_iter;

namespace {

    void
    copy_tags(strint_map& tag_occurrences, tag_map& tag_data)
    {
        strint_iter it(tag_occurrences.begin());
        strint_iter end(tag_occurrences.end());

        for ( ; it != end; ++it) {
            shared_ptr<TagData> data(new TagData);
            data->count = it->second;
            data->font_size = 0;
            tag_data[it->first] = data;
        }
    }

}

TagCloudModel::TagCloudModel(int min_font_size, int max_font_size,
                             strint_map& tags)
    :
    min_font_size_(min_font_size),
    max_font_size_(max_font_size)
{
    copy_tags(tags, tags_);

    stats_.reset(new CloudStats);
    update_tag_data();
}

TagCloudModel::TagCloudModel(int min_font_size, int max_font_size)
    :
    min_font_size_(min_font_size),
    max_font_size_(max_font_size)
{
    stats_.reset(new CloudStats);
    update_tag_data();
}

TagCloudModel::~TagCloudModel()
{
}

void
TagCloudModel::calculate_stats()
{
    stats_->font_range = max_font_size_ - min_font_size_;

    tag_iter it(tags_.begin());
    tag_iter end(tags_.end());

    if (it == end)
        return;

    int min_count = it->second->count;
    int max_count = it->second->count;

    for ( ; it != end; ++it) {
        if (it->second->count < min_count) {
            min_count = it->second->count;
            continue;
        }

        if (it->second->count > max_count)
            max_count = it->second->count;
    }

    stats_->min_tag_count = min_count;
    stats_->max_tag_count = max_count;

    stats_->min_log = log(min_count);
    stats_->max_log = log(max_count);

    if (stats_->max_log == stats_->min_log)
        stats_->log_range = 1;
    else
        stats_->log_range = stats_->max_log - stats_->min_log;
}

void
TagCloudModel::update_font_sizes()
{
    tag_iter it(tags_.begin());
    tag_iter end(tags_.end());

    for ( ; it != end; ++it) {
        double count_ratio =
            (log(it->second->count) - stats_->min_log) / stats_->log_range;

        it->second->font_size =
            min_font_size_ + static_cast<int>(stats_->font_range * count_ratio);
    }
}

void
TagCloudModel::update_tag_data()
{
    calculate_stats();
    update_font_sizes();
}

void
TagCloudModel::add_tag(const Glib::ustring& tag)
{
    if (tags_.find(tag) == tags_.end()) { // new tag
        shared_ptr<TagData> data(new TagData);
        data->count = 1;
        tags_[tag] = data;
    } else {
        tags_[tag]->count++;
    }

    update_tag_data();
    signal_changed_.emit();
}

void
TagCloudModel::remove_tag(const Glib::ustring& tag)
{
    tag_iter it(tags_.find(tag));
    if (it == tags_.end()) return;

    shared_ptr<TagData> data = it->second;
    if (--(data->count) == 0) tags_.erase(it);
    
    update_tag_data();
    signal_changed_.emit();
}

void
TagCloudModel::get_font_sizes(strint_map& tags)
{
    tag_iter it(tags_.begin());
    tag_iter end(tags_.end());

    for ( ; it != end; ++it)
        tags[it->first] = it->second->font_size;
}
