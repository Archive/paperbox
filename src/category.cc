/*
 *  Paperbox - category.cc
 *
 *  Copyright (C) 2007 Marko Anastasov
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include <errno.h>
#include <glib/gstdio.h>
#include <fstream>
#include <iostream>
#include <glibmm/fileutils.h>
#include <glibmm/miscutils.h>
#include <glibmm-utils/log-stream-utils.h>
#include "category.hh"

namespace {

    // Loads open text stream's contents into a string list.
    void read_lines(std::ifstream& fs,
                    std::vector<std::string>& lines)
    {
        std::string line;

        while (! fs.eof()) {
            std::getline(fs, line);
            if (! line.size()) continue;
            lines.push_back(line);
        }
    }

    // TODO: clean up the mess with strings vs ustrings.
    // Loads open text stream's contents into a string list.
    void read_lines(std::ifstream& fs,
                    std::vector<Glib::ustring>& lines)
    {
         std::string std_line;

         while (! fs.eof()) {
             std::getline(fs, std_line);
             if (! std_line.size()) continue;
             lines.push_back(std_line);
         }
    }
}

namespace paperbox {

    using std::string;
    using std::vector;

    // Checks for the main category directory, creates an empty file
    // if no previous data is present.
    Category::Category(const Glib::ustring& name)
        :
        name_(name.raw())
    {
        file_ = Gio::File::create_for_path(get_default_path() + name_);

        if (! file_->query_exists()) {
            // touch
            Glib::RefPtr<Gio::FileOutputStream> os = file_->create_file();
            os->close();
        } else {
            // load tags
            std::ifstream ifs(file_->get_path().c_str());
            read_lines(ifs, tags_);
        }
    }

    Category::~Category()
    {
    }

    bool
    Category::add_tag(const Glib::ustring& tag)
    {
        std::ifstream ifs(file_->get_path().c_str());

        if (! ifs.is_open()) {
            g_warning("Unable to open category file %s to add tag",
                      file_->get_path().c_str());
            return false;
        }

        vector<string> tags;
        read_lines(ifs, tags);

        if (find(tags.begin(), tags.end(), tag) != tags.end()) {
            g_warning("Tag %s already in category %s",
                      tag.c_str(), name_.c_str());
            return false;
        }

        Glib::RefPtr<Gio::FileOutputStream> stream = file_->append_to();
        stream->write(tag + '\n');
        if (! stream->close())
            LOG_ERROR("could not close stream for category file "
                      << file_->get_path());

        return true;
    }

    void
    Category::reset_tags(const vector<Glib::ustring>& new_tags)
    {
        Glib::RefPtr<Gio::FileOutputStream> stream;

        if (file_->query_exists())
            stream = file_->replace();
        else
            stream = file_->create_file();

        vector<Glib::ustring>::const_iterator it(new_tags.begin());
        vector<Glib::ustring>::const_iterator end(new_tags.end());
        for ( ; it != end; ++it)
            stream->write(*it + "\n");

        if (! stream->close())
            LOG_ERROR("could not close stream for category file "
                      << file_->get_path());
    }
 
    bool
    Category::remove_tag(const Glib::ustring& tag)
    {
        std::ifstream ifs(file_->get_path().c_str());

        if (! ifs.is_open()) {
            g_warning("Unable to open category file %s to remove tag",
                      file_->get_path().c_str());
            return false;
        }

        vector<string> tags;
        read_lines(ifs, tags);

        if (find(tags.begin(), tags.end(), tag.raw()) == tags.end()) {
            g_warning("Tag %s not found in category %s",
                      tag.c_str(), name_.c_str());
            return false;
        }

        tags.erase(remove(tags.begin(), tags.end(), tag.raw()), tags.end());

        // rewrite the file with remaining tags
        Glib::RefPtr<Gio::FileOutputStream> stream = file_->replace();

        vector<string>::iterator it(tags.begin());
        vector<string>::iterator end(tags.end());
        for ( ; it != end; ++it)
            stream->write(*it + "\n");

        if (! stream->close())
            LOG_ERROR("could not close stream for category file "
                      << file_->get_path());

        return true;
    }

    std::vector<Glib::ustring>
    Category::get_tags() const
    {
        return tags_;
    }

    Glib::ustring
    Category::get_tags_as_string() const
    {
        Glib::ustring tag_text;
        vector<Glib::ustring>::const_iterator it(tags_.begin());
        vector<Glib::ustring>::const_iterator end(tags_.end());

        if (it != end)
            tag_text = *it++;

        for ( ; it != end; ++it)
            tag_text = tag_text + " " + *it;

        return tag_text;
    }

    std::string
    Category::get_default_path()
    {
        return (Glib::get_home_dir() + G_DIR_SEPARATOR_S + ".config" +
                G_DIR_SEPARATOR + "paperbox" + G_DIR_SEPARATOR + CATEGORY_DIR +
                G_DIR_SEPARATOR);
    }

} // namespace paperbox
