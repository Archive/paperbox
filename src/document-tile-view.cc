/*
 *  Paperbox - document-tile-view.cc
 *
 *  Copyright (C) 2007 Marko Anastasov
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include <glib/gi18n.h>
#include "document.hh"
#include "document-tile-view.hh"

namespace paperbox {

    using std::vector;
    using std::tr1::shared_ptr;

    typedef vector<shared_ptr<Document> > doc_vector;

    DocumentTileView::DocumentTileView(bool use_page_view)
        :
        TileView(use_page_view)
    {
        Gtk::Util::PageNavigator& navigator = get_page_navigator();
        navigator.get_button_first().set_tooltip_text(_("First page"));
        navigator.get_button_previous().set_tooltip_text(_("Previous page"));
        navigator.get_button_next().set_tooltip_text(_("Next page"));
        navigator.get_button_last().set_tooltip_text(_("Last page"));
    }

    DocumentTileView::~DocumentTileView()
    {
    }

    void
    DocumentTileView::add_tile(Gtk::Util::Tile& tile)
    {
        add_tile(&tile);
    }

    void
    DocumentTileView::add_tile(Gtk::Util::Tile* tile)
    {
        DocumentTile* dtile = dynamic_cast<DocumentTile*>(tile);
        connect_to_extra_signals(dtile);

        TileView::add_tile(tile);
    }

    void
    DocumentTileView::connect_to_extra_signals(DocumentTile* tile)
    {
        tile->signal_tag_clicked().connect(
            sigc::mem_fun(*this, &DocumentTileView::on_tag_selected));
    }

    void
    DocumentTileView::on_tag_selected(const Glib::ustring& tag)
    {
        signal_tag_clicked_.emit(tag); // propagate further
    }

    void
    DocumentTileView::on_show_request()
    {
        // The following shows all document tiles and *all* their children.
        // The consequence is that eg after all have been packed,
        // ie after first selection, a tag selection would make all
        // these tiles appear completely expanded
        TileView::on_show_request();

        // but on each fresh show (tag selection, page navigation) we want
        // to hide the widgets with additional document information:
        for_each_tile(sigc::mem_fun(*this,
                                    &DocumentTileView::extra_info_hider));
    }

    void
    DocumentTileView::extra_info_hider(Gtk::Util::Tile& t)
    {
        DocumentTile* dt = static_cast<DocumentTile*>(&t);
        dt->hide_extra_info();
    }

    void
    DocumentTileView::tile_walker(Gtk::Util::Tile& tile,
                                  doc_vector* current_docs)
    {
        DocumentTile* dtile = dynamic_cast<DocumentTile*>(&tile);
        current_docs->push_back(dtile->get_document());
    }

    void
    DocumentTileView::get_displayed_documents(doc_vector& docs)
    {
        for_each_tile(sigc::bind(
            sigc::mem_fun(*this, &DocumentTileView::tile_walker),
            &docs));
    }

} // namespace paperbox
