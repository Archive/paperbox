/*
 * Paperbox - tag-cloud-model.hh
 *
 * Copyright (C) 2007 Marko Anastasov
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef __PAPERBOX_TAG_CLOUD_MODEL__
#define __PAPERBOX_TAG_CLOUD_MODEL__

#include <map>
#include <tr1/memory>
#include <glibmm/ustring.h>
#include <sigc++/signal.h>


typedef std::map<Glib::ustring, int> strint_map;
typedef strint_map::iterator strint_iter;

struct lt_ustring
{
    bool operator()(const Glib::ustring& s1, const Glib::ustring& s2) const
    {
        return s1.compare(s2) < 0;
    }
};

class CloudStats;
struct TagData;

typedef std::map<Glib::ustring, std::tr1::shared_ptr<TagData>, lt_ustring>
tag_map;

class TagCloudModel : public sigc::trackable
{
public:
    explicit TagCloudModel(int min_font_size, int max_font_size,
                           strint_map& tags);

    explicit TagCloudModel(int min_font_size, int max_font_size);

    virtual ~TagCloudModel();

    void add_tag(const Glib::ustring& tag);
    void remove_tag(const Glib::ustring& tag);

    void get_font_sizes(strint_map& tags);

    int get_min_font_size() const { return min_font_size_; }
    int get_max_font_size() const { return max_font_size_; }

    sigc::signal<void>& signal_changed() { return signal_changed_; }

protected:
    void calculate_stats();
    void update_font_sizes();
    void update_tag_data();

    sigc::signal<void> signal_changed_;

    int min_font_size_;
    int max_font_size_;
    std::tr1::shared_ptr<CloudStats> stats_;

    tag_map tags_;

private:
    TagCloudModel(const TagCloudModel& );
    TagCloudModel& operator=(const TagCloudModel& );
};

#endif // __PAPERBOX_TAG_CLOUD_MODEL__
