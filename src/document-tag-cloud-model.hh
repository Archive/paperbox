/*
 * Paperbox - document-tag-cloud-model.hh
 *
 * Copyright (C) 2007 Marko Anastasov
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef __PAPERBOX_DOCUMENT_TAG_CLOUD_MODEL_H__
#define __PAPERBOX_DOCUMENT_TAG_CLOUD_MODEL_H__

#include <string>
#include <tr1/memory>
#include <vector>
#include <glibmm/ustring.h>
#include "tag-cloud-model.hh"

namespace paperbox {

    class Document;

    class DocumentTagCloudModel : public TagCloudModel
    {
    public:
        explicit DocumentTagCloudModel(int min_font_size, int max_font_size);
        virtual ~DocumentTagCloudModel();

        virtual void update_tags(const std::tr1::shared_ptr<Document>& doc);

    protected:
        void on_tags_changed(const std::string& uri,
                             const std::vector<Glib::ustring>& tags_added,
                             const std::vector<Glib::ustring>& tags_removed);
    };

} // namespace paperbox

#endif // __PAPER_BOXDOCUMENT_TAG_CLOUD_MODEL_H__
