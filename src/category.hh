/*
 *  Paperbox - category.hh
 *
 *  Copyright (C) 2007 Marko Anastasov
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#ifndef __PAPER_BOX_CATEGORY_HH__
#define __PAPER_BOX_CATEGORY_HH__

#include <string>
#include <vector>
#include <glibmm/ustring.h>
#include <giomm/file.h>
#include "noncopyable.hh"

namespace paperbox {

    const std::string CATEGORY_DIR = "categories";

    class Category : private NonCopyable
    {
    public:
        explicit Category(const Glib::ustring& name);
        virtual ~Category();

        bool add_tag(const Glib::ustring& tag);
        void reset_tags(const std::vector<Glib::ustring>& new_tags);
        bool remove_tag(const Glib::ustring& tag);

        std::string get_name() const;
        std::vector<Glib::ustring> get_tags() const;
        Glib::ustring get_tags_as_string() const;

        static std::string get_default_path();

    protected:
        std::string name_;
        Glib::RefPtr<Gio::File> file_;
        std::vector<Glib::ustring> tags_;
    };

    inline std::string
    Category::get_name() const
    {
        return name_;
    }

} // namespace paperbox

#endif // __PAPER_BOX_CATEGORY_HH__
