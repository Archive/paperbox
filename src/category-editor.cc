/*
 *  Paperbox - category-editor.cc
 *
 *  Copyright (C) 2008 Marko Anastasov
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include <list>
#include <glib/gi18n.h>
#include <gtkmm/stock.h>
#include <glibmm-utils/log-stream-utils.h>
#include <gtkmm-utils/dialog.h>
#include "browser.hh"
#include "category-editor.hh"
#include "category-editor-model.hh"
#include "dialog-entry.hh"
#include "document.hh"
#include "document-tag-cloud-model.hh"
#include "paths.hh"

namespace paperbox {

    using std::list;
    using std::vector;
    using std::tr1::shared_ptr;

    const int DEFAULT_WIDTH = 500;
    const int DEFAULT_HEIGHT = 375; // try to keep the ratio 4:3

    const int TAG_FONT_SIZE = 10;

    CategoryEditor::CategoryEditor(
        GtkDialog* cobject,
        const Glib::RefPtr<Gnome::Glade::Xml>& glade)
        :
        Gtk::Dialog(cobject),
        glade_(glade),
        button_new_(Gtk::Stock::NEW),
        button_delete_(Gtk::Stock::DELETE),
        button_save_(Gtk::Stock::SAVE),
        initial_tag_load_complete_(false)
    {
        init_gui();
        set_title(_("Category editor"));
        connect_signals();
        model_.reset(new CategoryEditorModel());
        load_categories();
    }

    CategoryEditor::~CategoryEditor()
    {
    }

    CategoryEditor*
    CategoryEditor::create()
    {
        Glib::RefPtr<Gnome::Glade::Xml> glade_xml =
            Gnome::Glade::Xml::create(paperbox::glade_dialog_categories);

        CategoryEditor* p = 0;
        glade_xml->get_widget_derived("dialog_categories", p);
        return p;
    }

    void
    CategoryEditor::get_widgets()
    {
        glade_->get_widget("vbox_left", vbox_left_);
        g_assert(vbox_left_);

        glade_->get_widget("vbox_right", vbox_right_);
        g_assert(vbox_right_);

        glade_->get_widget("hbox_contents", hbox_contents_);
        g_assert(hbox_contents_);
    }

    void
    CategoryEditor::init_gui()
    {
        get_widgets();

        bool no_edit_button = false;
        category_view_.reset(new CategoryView(vbox_left_, no_edit_button));

        vbox_left_->pack_start(button_new_, false, false);
        vbox_left_->pack_start(button_delete_, false, false);

        label_category_tags_.set_text(_("Tags contained:"));
        hbox_contents_->pack_start(label_category_tags_);
        hbox_contents_->pack_start(button_save_, false, false);
        button_save_.set_sensitive(false);

        vbox_right_->pack_start(scroll_window_);
        scroll_window_.set_policy(Gtk::POLICY_NEVER, Gtk::POLICY_AUTOMATIC);
        scroll_window_.add(text_view_);
        text_view_.set_wrap_mode(Gtk::WRAP_WORD);

        vbox_right_->pack_start(tag_box_);
        tag_cloud_model_.reset(
            new DocumentTagCloudModel(TAG_FONT_SIZE, TAG_FONT_SIZE));
        tag_cloud_.set_model(tag_cloud_model_);
        tag_cloud_.set_lighten_rare_tags(false);
        tag_box_.pack_start(tag_cloud_, true, true);
        tag_cloud_.set_mark_selected_tags(false);

        set_default_size(DEFAULT_WIDTH, DEFAULT_HEIGHT);

        show_all_children();
    }

    void
    CategoryEditor::connect_signals()
    {
        button_new_.signal_clicked().connect(
            sigc::mem_fun(*this, &CategoryEditor::on_button_new_clicked));

        button_delete_.signal_clicked().connect(
            sigc::mem_fun(*this, &CategoryEditor::on_button_delete_clicked));

        button_save_.signal_clicked().connect(
            sigc::mem_fun(*this, &CategoryEditor::on_button_save_clicked));

        // connect the selection function
        category_view_->selection->set_select_function(
            sigc::mem_fun(*this, &CategoryEditor::on_category_selected));

        // monitor text view
        text_view_.signal_key_release_event().connect(
            sigc::mem_fun(*this, &CategoryEditor::on_key_release_event));

        // get notified about new tags through new documents
        // TODO: what about signals for tags added/removed?
        Browser::instance()->signal_document_retrieved().connect(
            sigc::mem_fun(*this, &CategoryEditor::on_document_retrieved));

        tag_cloud_.signal_tag_clicked().connect(
            sigc::mem_fun(*this, &CategoryEditor::on_tag_clicked));
    }

    int
    CategoryEditor::run()
    {
        int response = Gtk::Dialog::run();
        return response;
    }

    void
    CategoryEditor::add_new_row(shared_ptr<CategoryEditorData>& data)
    {
        // update our view - create a new row, set the buffer
        Gtk::TreeModel::Row row = *(category_view_->treemodel->append());

        row[category_view_->columns.col_name] = data->category->get_name();
        category_view_->selection->select(row);

        text_view_.set_buffer(data->buffer);
    }

    void
    CategoryEditor::load_categories()
    {
        list<shared_ptr<CategoryEditorData> > cats(
            model_->load_category_data());

        list<shared_ptr<CategoryEditorData> >::iterator it(cats.begin());
        list<shared_ptr<CategoryEditorData> >::iterator end(cats.end());

        for ( ; it != end; ++it)
            add_new_row(*it);

        category_view_->select_first();
    }

    void
    CategoryEditor::on_button_new_clicked()
    {
        shared_ptr<DialogEntry> dialog(DialogEntry::create());

        dialog->set_instructions(_("Name of the new category:"));
        dialog->set_default_response(Gtk::RESPONSE_OK);

        Glib::ustring name;
        int response = dialog->run(name);

        if (response != Gtk::RESPONSE_OK) return;

        try {
            // create a new data model and category file
            shared_ptr<CategoryEditorData> data = model_->new_category(name);

            add_new_row(data);

            if (category_view_->treemodel->children().size() == 1) {
                // first category created,
                // so previously we had some widgets disabled
                text_view_.set_sensitive();
                button_save_.set_sensitive();
            }
        } catch (const CategoryExists& ex) {
            Gtk::Util::display_dialog_error(ex.what());
        }
    }

    void
    CategoryEditor::on_button_delete_clicked()
    {
        Gtk::TreeModel::iterator row =
            category_view_->selection->get_selected();

        if (! row) return;

        // delete from both...
        category_view_->treemodel->erase(row);
        model_->delete_category(selected_name_);

        if (category_view_->treemodel->children().begin() != 
            category_view_->treemodel->children().end()) {
            // select the first row
            category_view_->selection->select(
                category_view_->treemodel->children().begin());
        } else {
            // last row deleted, assign a dummy buffer to the textview
            text_view_.set_buffer(Gtk::TextBuffer::create());

            // disable related widgets
            text_view_.set_sensitive(false);
            button_save_.set_sensitive(false);
        }
    }

    bool
    CategoryEditor::on_category_selected(const Glib::RefPtr<Gtk::TreeModel>& ,
                                         const Gtk::TreeModel::Path& path,
                                         bool path_selected)
    {
        Gtk::TreeModel::Row row = *(category_view_->treemodel->get_iter(path));
        Glib::ustring cat_name = row[category_view_->columns.col_name];

        if (! ((cat_name != selected_name_) && (! path_selected)))
            return true; // selection hasn't changed

        selected_name_ = cat_name;

        try {
            shared_ptr<CategoryEditorData> data(model_->get_category(cat_name));
            text_view_.set_buffer(data->buffer);
            check_buffer_status();
        } catch (const CategoryNotFound& ex) {
            // this would be rather odd, someone would need to mess with
            // the files in $config, but here goes
            Gtk::Util::display_dialog_error(ex.what());
        }

        return true;
    }

    void
    CategoryEditor::on_button_save_clicked()
    {
        model_->save_category(selected_name_);
        button_save_.set_sensitive(false);
    }

    bool
    CategoryEditor::on_key_release_event(GdkEventKey* /* key */)
    {
        check_buffer_status();
        return true;
    }

    void
    CategoryEditor::on_document_retrieved(const shared_ptr<Document>& doc)
    {
        static_cast<DocumentTagCloudModel*>(
            tag_cloud_model_.get())->update_tags(doc);
    }

    void
    CategoryEditor::load_tags()
    {
        vector<shared_ptr<Document> > docs;
        Browser::instance()->get_all_documents(docs);

        vector<shared_ptr<Document> >::iterator it(docs.begin());
        vector<shared_ptr<Document> >::iterator end(docs.end());
        for ( ; it != end; ++it)
            static_cast<DocumentTagCloudModel*>(
                tag_cloud_model_.get())->update_tags(*it);
    }

    bool
    CategoryEditor::on_expose_event(GdkEventExpose* event)
    {
        // we need to wait for the window to be drawn as tag cloud
        // expects its parent's dimensions to be available
        if (! initial_tag_load_complete_) {
            load_tags();
            initial_tag_load_complete_ = true;
        }

        Gtk::Widget* child_widget = get_child(); // Gtk::Bin method
        if (child_widget)
            propagate_expose(*child_widget, event); // Gtk::Container

        return false;
    }

    void
    CategoryEditor::on_tag_clicked(const Glib::ustring& tag)
    {
        using Glib::ustring;

        Glib::RefPtr<Gtk::TextBuffer> buf = text_view_.get_buffer();
        ustring text = buf->get_text();
        ustring::size_type pos = text.find(tag);

        if (pos == ustring::size_type(-1))
            text.insert(text.size(), " " + tag);
        else {
            ustring::size_type len = tag.size();
            if (pos > 0) { // erase the leading whitespace as well
                --pos;
                ++len;
            }
            text = text.erase(pos, len);
        }

        buf->set_text(text);
        check_buffer_status();
    }

    void
    CategoryEditor::check_buffer_status()
    {
        shared_ptr<CategoryEditorData> data(
            model_->get_category(selected_name_));
        if (! data.get()) {
            //TODO: This happens when there are no categories defined.
            // At that point there is still no buffer.
            LOG_DD("Trying to do something with an empty buffer.");
            return;
        }

        (data->category->get_tags_as_string() != data->buffer->get_text())
            ? button_save_.set_sensitive()
            : button_save_.set_sensitive(false);
    }

} // namespace paperbox
