/*
 *  Paperbox - tracker-phone.hh
 *
 *  Copyright (C) 2007-2008 Marko Anastasov
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#ifndef __PAPERBOX_TRACKER_PHONE__
#define __PAPERBOX_TRACKER_PHONE__

#include <queue>
#include <stdexcept>
#include <tr1/memory>
#include <vector>
#include <glibmm/ustring.h>
#include <sigc++/sigc++.h>
#include <tracker.h>
#include "document.hh"
#include "noncopyable.hh"

namespace paperbox {

    // It's not called TrackerClient because
    // libtracker defines a TrackerClient struct.

    class TrackerPhone : private NonCopyable
    {
    public:
        typedef sigc::signal<void,
                             const std::queue<Glib::ustring>& > // uri queue
        SignalDocuments;

        typedef sigc::signal<void,
                             const Glib::ustring&,               // uri
                             const std::vector<Glib::ustring>& > // tags
        SignalTags;

        explicit TrackerPhone() throw (std::runtime_error);
        virtual ~TrackerPhone();

        static bool ping_tracker();

        void request_all_documents();

        void get_document(const Glib::ustring& uri,
                          std::tr1::shared_ptr<Document>& document);

        void add_tags(const Glib::ustring& uri,
                      const std::vector<Glib::ustring> tags);

        void remove_tags(const Glib::ustring& uri,
                         const std::vector<Glib::ustring> tags);

        SignalDocuments& signal_documents() { return signal_documents_; }

        SignalTags& signal_tags_added() { return signal_tags_added_; }

        SignalTags& signal_tags_removed() { return signal_tags_removed_; }

    protected:
        void connect_to_dbus_signals();

        void prepare_document(const Glib::ustring& uri,
                              char** keys,
                              std::tr1::shared_ptr<Document>& document);

        void attach_tags(std::tr1::shared_ptr<Document>& document);

        TrackerClient* tracker_connection_;
        DBusGConnection* bus_;
        DBusGProxy* remote_kw_object_;

        SignalDocuments signal_documents_;
        SignalTags signal_tags_added_;
        SignalTags signal_tags_removed_;
    };

} // namespace paperbox

#endif // __PAPERBOX_TRACKER_PHONE__
