/*
 * Paperbox - tag-cloud.cc
 *
 * Copyright (C) 2007 Marko Anastasov
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <map>
#include <sstream>
#include <goocanvas.h>
#include "tag-cloud.hh"
#include "tag-item.hh"

namespace paperbox {

using std::list;
using std::map;
using std::tr1::shared_ptr;

static const guint32 TAG_SPACING = 8;
static const guint32 TAG_BORDER_MIN = 5;
static const guint32 TAG_BORDER_MAX = TAG_BORDER_MIN * 4;

static gboolean
on_button_press (GooCanvasItem*  /* item */,
                 GooCanvasItem*  /* target */,
                 GdkEventButton* /* event */,
                 TagItem*        tag_item)
{
    TagCloud::SignalTagClicked& signal =
        tag_item->get_parent()->signal_tag_clicked();
    signal.emit(tag_item->get_tag());

    tag_item->get_parent()->update_marker(tag_item);

    return TRUE;
}

static gboolean
on_enter_notify (GooCanvasItem* item,
		 GooCanvasItem*  /* target */,
		 GdkEventMotion* /* event */,
		 TagItem*        /* tag_item */)
{
    g_object_set (item, "fill_color_rgba", FILL_COLOR_RED, NULL);

    return FALSE;
}

static gboolean
on_leave_notify (GooCanvasItem* item,
		 GooCanvasItem*  /* target */,
		 GdkEventMotion* /* event */,
		 TagItem* tag_item)
{
    g_object_set (item,
                  "fill_color_rgba", tag_item->get_fill_colour(),
                  NULL);

    return FALSE;
}

TagCloud::TagCloud()
    :
    canvas_(0),
    selection_(0),
    grand_(0),
    randomize_borders_(true),
    underline_tags_(false),
    lighten_rare_tags_(true),
    mark_selected_(true),
    alloc_width_(0),
    alloc_height_(0)
{
    set_shadow_type(Gtk::SHADOW_IN);
    set_policy (Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC);

    grand_ = g_rand_new();
}

TagCloud::~TagCloud()
{
    remove_canvas_items();
    delete canvas_;
    g_rand_free (grand_);
}

void
TagCloud::on_size_allocate(Gdk::Rectangle& alloc)
{
    if (! canvas_) {
        canvas_ = Glib::wrap(goo_canvas_new());
        add(*canvas_);
        canvas_->show();
    }

    bool redraw = ((alloc_width_ != alloc.get_width()) ||
                   (alloc_height_ != alloc.get_height()));

    if (! redraw) {
        Gtk::ScrolledWindow::on_size_allocate(alloc);
        return;
    }

    alloc_width_ = alloc.get_width();
    alloc_height_ = alloc.get_height();

    redraw_tags();

    Gtk::ScrolledWindow::on_size_allocate(alloc);
}

void
TagCloud::set_model(shared_ptr<TagCloudModel> model)
{
    model_ = model;
    model_->signal_changed().connect(
        sigc::mem_fun(*this, &TagCloud::on_model_changed));
}

void
TagCloud::set_tags(const std::map<Glib::ustring, int>& tags)
{
    if (tags.empty()) return;

    double canvas_left, canvas_top, canvas_right, canvas_bottom;

    goo_canvas_get_bounds(GOO_CANVAS (canvas_->gobj()),
                          &canvas_left, &canvas_top,
                          &canvas_right, &canvas_bottom);

    const int visible_width = static_cast<int>(canvas_right - canvas_left);

    double line_height =
        model_->get_max_font_size() * 1.5; // I think this is a safe guess

    std::map<Glib::ustring, int>::const_iterator it(tags.begin());
    std::map<Glib::ustring, int>::const_iterator end(tags.end());
    GooCanvasItem* text_item, * root;

    root = goo_canvas_get_root_item(GOO_CANVAS (canvas_->gobj()));

    int bold_color_limit = 0;
    if (lighten_rare_tags_)
        bold_color_limit =
            (model_->get_min_font_size() + 
             (model_->get_max_font_size() - model_->get_min_font_size()) / 2);

    int current_line = 0;
    int tags_in_current_line = 0;
    double text_item_x, text_item_y;
    GooCanvasBounds bounds;
    int text_item_width = 0;

    int last_border = TAG_BORDER_MIN;

    text_item_x = TAG_BORDER_MIN;

    for ( ; it != end; ++it) {
        std::ostringstream os;
        os << it->second;
        Glib::ustring font = "Bitstream vera sans " + os.str();

        text_item_y = (current_line + 1) * line_height;

        Glib::ustring text(it->first);
        if (underline_tags_)
            text = "<u>" + text + "</u>";

        guint fill_color = FILL_COLOR_BLUE;
        if (lighten_rare_tags_ && (it->second <= bold_color_limit))
            fill_color = FILL_COLOR_LIGHT_BLUE;

        text_item = goo_canvas_text_new (root,
                                         text.c_str(),
					 text_item_x,
                                         text_item_y,
                                         -1,
					 GTK_ANCHOR_WEST,
					 "fill-color-rgba", fill_color,
					 "font", font.c_str(),
                                         "use-markup", TRUE,
					 NULL);

        // Determine the width of the created text item, and will it fit in the
        // current line. We start a new line when we have at least one tag
        // in the current, and the new one is too large for the available
        // visible space in the scrolled window.
	goo_canvas_item_get_bounds (text_item, &bounds);
        text_item_width = static_cast<int>(bounds.x2 - bounds.x1);
        text_item_x += text_item_width;

        if (((text_item_x + last_border) > visible_width) &&
            (tags_in_current_line >= 1)) {
            // Set the coordinates to the beginning of a new line.
            ++current_line;

            if (randomize_borders_) {
                last_border = g_rand_int_range(grand_,
                                               TAG_BORDER_MIN,
                                               TAG_BORDER_MAX);
            }

            text_item_x = last_border;
            text_item_y = (current_line + 1) * line_height;

            tags_in_current_line = 0;

            // If the canvas is too small vertically or horizontally
            // to accept a new text item, expand it accordingly.
            if (text_item_y > (canvas_bottom - canvas_top)) {
                canvas_bottom += line_height;
                goo_canvas_set_bounds(GOO_CANVAS (canvas_->gobj()),
                                      canvas_left, canvas_top,
                                      canvas_right, canvas_bottom);
            }

            if (text_item_width > (canvas_right - canvas_left)) {
                canvas_right = text_item_width + (TAG_SPACING + last_border);
                goo_canvas_set_bounds(GOO_CANVAS (canvas_->gobj()),
                                      canvas_left, canvas_top,
                                      canvas_right, canvas_bottom);
            }

            // Move the item to the beginning of the new line.
            g_object_set(GOO_CANVAS_ITEM(text_item),
                         "x", text_item_x,
                         "y", text_item_y,
                         NULL);

            text_item_x += text_item_width + TAG_SPACING;
        }
        else
            text_item_x += TAG_SPACING;

        ++tags_in_current_line;

        shared_ptr<TagItem> item(
            new TagItem(this,
                        text_item,
                        it->first,
                        static_cast<FillColor>(fill_color)));

        text_items_.push_back(item);

        // Connect to text item's signals.
	g_signal_connect (text_item, "button_press_event",
			  (GtkSignalFunc) on_button_press, item.get());

	g_signal_connect (text_item, "enter_notify_event",
			  (GtkSignalFunc) on_enter_notify, item.get());

	g_signal_connect (text_item, "leave_notify_event",
			  (GtkSignalFunc) on_leave_notify, item.get());
    }
}

void
TagCloud::remove_canvas_items()
{
    list<shared_ptr<TagItem> >::iterator item_it(text_items_.begin());
    list<shared_ptr<TagItem> >::iterator item_end(text_items_.end());

    for ( ; item_it != item_end; ++item_it) {
        goo_canvas_item_remove ((*item_it)->get_canvas_item());
        GooCanvasItem* marker = (*item_it)->get_marker();
        if (marker) {
            goo_canvas_item_remove (marker);
            marker = 0;
        }
    }
}

void
TagCloud::redraw_tags()
{
    reset_selection(true); // we need to invalidate the marker
    remove_canvas_items();
    text_items_.clear();

    canvas_->set_size_request(alloc_width_ - 30,
                              alloc_height_ - 30);

    goo_canvas_set_bounds (GOO_CANVAS (canvas_->gobj()),
                           0, 0,
                           alloc_width_ - 30, alloc_height_ - 30);

    strint_map tags;
       
    model_->get_font_sizes(tags);
    set_tags(tags);

    if (selection_string_.size()) {
        list<shared_ptr<TagItem> >::iterator it(text_items_.begin());
        list<shared_ptr<TagItem> >::iterator end(text_items_.end());

        for ( ; it != end; ++it)
            if ((*it)->get_tag() == selection_string_)
                update_marker(it->get());
    }
}

void
TagCloud::on_model_changed()
{
    redraw_tags();
}

void
TagCloud::update_marker(TagItem* new_selection)
{
    if (mark_selected_) {
        if (selection_) selection_->marker_off();
        new_selection->marker_on();
    }

    selection_ = new_selection;
    selection_string_ = selection_->get_tag();
}

/// Set keep_old_selection to true to keep the selection_string_ variable
/// unchanged - useful when we need to redraw a selection marker around a tag
/// which had been selected before the cloud redraw.
void
TagCloud::reset_selection(bool keep_old_selection)
{
    if (! selection_) return;

    selection_->marker_off();
    selection_ = 0;

    if (! keep_old_selection)
        selection_string_ = "";
}

// Finds a TagItem by string and causes its' rectangle marker to be drawn.
void
TagCloud::select_tag(const Glib::ustring& tag)
{
    reset_selection();

    list<shared_ptr<TagItem> >::iterator item_it(text_items_.begin());
    list<shared_ptr<TagItem> >::iterator item_end(text_items_.end());

    for ( ; item_it != item_end; ++item_it) {
        if ((*item_it)->get_tag() == tag) {
            (*item_it)->marker_on();
            selection_ = item_it->get();
            selection_string_ = tag;
            break;
        }
    }
}

} // namespace paperbox
