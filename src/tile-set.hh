/*
 *  Paperbox - tile-set.hh
 *
 *  Copyright (C) 2007 Marko Anastasov
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#ifndef __PAPERBOX_TILE_SET_HH__
#define __PAPERBOX_TILE_SET_HH__

#include <map>
#include <string>
#include <tr1/memory>
#include "document-tile.hh"
#include "noncopyable.hh"

namespace paperbox {

    /**
     * Data about displayed document tiles
     */
    class TileSet : private NonCopyable
    {
    public:
        explicit TileSet();
        ~TileSet();

        bool add(std::tr1::shared_ptr<DocumentTile>& tile);

        std::tr1::shared_ptr<DocumentTile> get_tile(const std::string& uri);

    protected:
        typedef std::map<std::string,
                         std::tr1::shared_ptr<DocumentTile>,
                         std::less<std::string> >
        tile_map;

        tile_map tiles_;
    };

} // namespace paperbox

#endif // __PAPER_BOX_TILE_SET_HH__
