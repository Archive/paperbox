/*
 *  Paperbox - dialog-entry.hh
 *
 *  Copyright (C) 2007 Marko Anastasov
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#ifndef __PAPERBOX_DIALOG_ENTRY_HH__
#define __PAPERBOX_DIALOG_ENTRY_HH__

#include <gtkmm/dialog.h>
#include <gtkmm/entry.h>
#include <gtkmm/label.h>
#include <libglademm/xml.h>

namespace paperbox {

    class DialogEntry : public Gtk::Dialog
    {
    public:
        DialogEntry(GtkDialog* cobject,
                    const Glib::RefPtr<Gnome::Glade::Xml>& glade);
        virtual ~DialogEntry();

        static DialogEntry* create();

        virtual void set_instructions(const Glib::ustring& msg);

        int run(Glib::ustring& text);

    protected:
        Glib::RefPtr<Gnome::Glade::Xml> glade_;
        Gtk::Label* label_instructions_;
        Gtk::Entry* entry_tags_;
    };

} // namespace paperbox

#endif // __PAPER_BOXDIALOG_ENTRY_HH__
