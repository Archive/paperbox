/*
 *  Paperbox - category-view.hh
 *
 *  Copyright (C) 2008 Marko Anastasov
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#ifndef __PAPERBOX_CATEGORY_VIEW__
#define __PAPERBOX_CATEGORY_VIEW__

#include <gtkmm/box.h>
#include <gtkmm/label.h>
#include <gtkmm/liststore.h>
#include <gtkmm/scrolledwindow.h>
#include <gtkmm/treeview.h>
#include "noncopyable.hh"

namespace paperbox {

    // treemodel
    class CategoryModelColumns : public Gtk::TreeModel::ColumnRecord
    {
    public:
        CategoryModelColumns() { add(col_id); add(col_name); }
        
        Gtk::TreeModelColumn<unsigned int> col_id; // not displayed
        Gtk::TreeModelColumn<Glib::ustring> col_name;
    };

    /// common treeview setup for categories
    class CategoryView : private NonCopyable
    {
    public:
        explicit CategoryView(Gtk::Box* parent, bool edit_enabled = true);
        virtual ~CategoryView() {}

        virtual void select_first();

        Gtk::Box* parent;
        Gtk::HBox title_box;
        Gtk::Button button_edit;
        Gtk::Label label_title;
        Gtk::ScrolledWindow scroll;
        Gtk::TreeView treeview;
        CategoryModelColumns columns;
        Glib::RefPtr<Gtk::ListStore> treemodel;
        Glib::RefPtr<Gtk::TreeSelection> selection;
    };

} // namespace paperbox

#endif // __PAPERBOX_CATEGORY_VIEW__
