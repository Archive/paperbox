/*
 *  Paperbox - category-editor-model.cc
 *
 *  Copyright (C) 2008 Marko Anastasov
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include <vector>
#include <glib/gi18n.h>
#include <glibmm-utils/log-stream-utils.h>
#include <glibmm-utils/ustring.h>
#include "category-editor-model.hh"

namespace paperbox {

    using std::list;
    using std::map;
    using std::vector;
    using std::tr1::shared_ptr;

    typedef map<Glib::ustring,
                shared_ptr<CategoryEditorData> >::iterator
    model_data_iter;

    ///

    CategoryEditorData::CategoryEditorData(const Glib::ustring& name)
        :
        dirty(false)
    {
        category = CategoryFactory::create_category(name);
        buffer = Gtk::TextBuffer::create();
    }

    CategoryEditorData::CategoryEditorData(shared_ptr<Category>& cat)
        :
        dirty(false)
    {
        category = cat;
        buffer = Gtk::TextBuffer::create();
        buffer->set_text(category->get_tags_as_string());
    }

    ///

    CategoryEditorModel::CategoryEditorModel()
    {
    }

    CategoryEditorModel::~CategoryEditorModel()
    {
    }

    // loads data for all existing categories from the fs
    list<shared_ptr<CategoryEditorData> > 
    CategoryEditorModel::load_category_data()
    {
        list<shared_ptr<CategoryEditorData> > data;
        list<shared_ptr<Category> > categories(
            CategoryFactory::load_categories());

        list<shared_ptr<Category> >::iterator it(categories.begin());
        list<shared_ptr<Category> >::iterator end(categories.end());

        for ( ; it != end; ++it) {
            shared_ptr<CategoryEditorData> ptr(new CategoryEditorData(*it));
            data.push_back(ptr);
            model_data_[(*it)->get_name()] = ptr;
        }

        return data;
    }

    shared_ptr<CategoryEditorData>
    CategoryEditorModel::new_category(const Glib::ustring& name)
    {
        CategoryEditorData* data = 0;
        shared_ptr<CategoryEditorData> ptr;

        if (! CategoryFactory::is_name_available(name))
            throw CategoryExists(
                Glib::Util::uprintf(_("Category %s already exists"),
                                    name.c_str()));

        data = new CategoryEditorData(name);
        ptr.reset(data);
        model_data_[name] = ptr;

        return ptr;
    }

    shared_ptr<CategoryEditorData>
    CategoryEditorModel::get_category(const Glib::ustring& name)
    {
        shared_ptr<CategoryEditorData> ptr;
        model_data_iter it = model_data_.find(name);

        if (it != model_data_.end())
            ptr = it->second;

        return ptr;
    }

    void
    CategoryEditorModel::save_category(const Glib::ustring& name)
    {
        shared_ptr<CategoryEditorData> ptr = get_category(name);
        vector<Glib::ustring> new_tags(
            Glib::Util::split(ptr->buffer->get_text()));
        ptr->category->reset_tags(new_tags);
        ptr->dirty = false;
    }

    void
    CategoryEditorModel::delete_category(const Glib::ustring& name)
    {
        try {
            CategoryFactory::delete_category(name);
            model_data_.erase(name);
        } catch (const CategoryNotFound& ex) {
            LOG_EXCEPTION(ex.what());
        }
    }

} // namespace paperbox
