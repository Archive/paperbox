/*
 *  Paperbox - document-tile.hh
 *
 *  Copyright (C) 2007-2008 Marko Anastasov
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#ifndef __PAPERBOX_DOCUMENT_TILE_HH__
#define __PAPERBOX_DOCUMENT_TILE_HH__

#include <map>
#include <string>
#include <tr1/memory>
#include <gtkmm/button.h>
#include <gtkmm/image.h>
#include <gtkmm/label.h>
#include <gtkmm/linkbutton.h>
#include <gtkmm-utils/tile.h>
#include "document.hh"
#include "tag-link-button.hh"
#include "thumbnailer.hh"

namespace paperbox {

    // Definition of the hash map where we'll store dynamically created
    // tag link widgets. We use one widget per tag.
    typedef std::map<std::string,
                     std::tr1::shared_ptr<Gtk::Widget>,
                     std::less<std::string> >
    tag_link_map;

    // The map for tag labels. Again, one label per tag.
    typedef tag_link_map tag_label_map;

    class DocumentTile : public Gtk::Util::Tile
    {
    public:
        typedef sigc::signal<void, const Glib::ustring&> SignalTagClicked;

        explicit DocumentTile(Thumbnailer& thumbnailer,
                              const std::tr1::shared_ptr<Document>& doc);
        ~DocumentTile();

        virtual Glib::ustring get_document_uri() const;
        virtual std::string   get_document_uri_raw() const;

        virtual std::tr1::shared_ptr<Document> get_document();

        // The signal that forwards the event of clicking on a tag
        // within the tile to the view (DocumentTileView).
        virtual SignalTagClicked& signal_tag_clicked();

        // Hides additional containers and widgets.
        virtual void hide_extra_info();

        // The add tag action can be requested from a eg menu item too.
        virtual void show_add_tag_dialog();

    protected:
        virtual void connect_thumbnailer_signals(Thumbnailer* thumbnailer);
        virtual void connect_signals();

        virtual Gtk::Widget* get_tag_link(const std::string& tag);
        virtual Gtk::Widget* get_tag_label(const std::string& tag);

        virtual void pack_tag_links();
        virtual void unpack_tag_links();

        virtual void refresh_tag_links(
            const std::vector<Glib::ustring>& tags_added,
            const std::vector<Glib::ustring>& tags_removed);

        virtual void pack_tag_labels();
        virtual void unpack_tag_labels();

        virtual void refresh_tag_labels(
            const std::vector<Glib::ustring>& tags_added,
            const std::vector<Glib::ustring>& tags_removed);

        // Gtk::Widget
        virtual void on_show();

        // Tile overrides
        virtual void on_selected();
        virtual void on_unselected();

        // Browser's signal handlers

        void on_document_retrieved(const std::tr1::shared_ptr<Document>& doc);

        void on_tags_changed(const std::string& uri,
                             const std::vector<Glib::ustring>& tags_added,
                             const std::vector<Glib::ustring>& tags_removed);

        // Handler for Thumbnailer's signal
        void on_large_thumbnail_ready(const Glib::RefPtr<Gdk::Pixbuf>& pixbuf);
        void on_small_thumbnail_ready(const Glib::RefPtr<Gdk::Pixbuf>& pixbuf);

        // Handler for the "+" button for adding tags
        virtual void on_tag_add_clicked();

        // Handler for all linkbutton tag clicks
        virtual void on_taglink(Gtk::LinkButton* button,
                                const Glib::ustring& uri);

        virtual void on_tag_link_tag_remove_request(const Glib::ustring& tag);

        virtual void break_title();
        virtual void restore_title();

        Glib::ustring uri_;
        std::tr1::shared_ptr<Document> doc_;

        Thumbnailer* thumbnailer_;

        SignalTagClicked signal_tag_clicked_;

        Glib::RefPtr<Gdk::Pixbuf> thumbnail_small_;
        Glib::RefPtr<Gdk::Pixbuf> thumbnail_large_;
        Gtk::HBox   extra_hbox_;
        Gtk::Label  pages_label_;
        Gtk::HBox   time_hbox_;
        Gtk::Label  modtime_label_;
        Gtk::HBox   tag_hbox_;
        Gtk::HBox   tag_labels_hbox_;
        Gtk::HBox   tag_links_hbox_;
        Gtk::HBox   tag_tools_hbox_;
        Gtk::Label  tags_caption_;
        Gtk::Button button_add_tags_;
        Gtk::Image  image_add_;

        tag_link_map tag_links_;
        tag_label_map tag_labels_;

        bool first_boot_;
    };

} // namespace paperbox

#endif // __PAPERBOX_DOCUMENT_TILE_HH__
