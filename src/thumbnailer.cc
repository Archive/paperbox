/*
 *  Paperbox - thumbnailer.cc
 *
 *  Copyright (C) 2007 Marko Anastasov
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include <map>
#include <queue>
#include <string>
#include <gtk/gtkicontheme.h>
#include <libgnomevfs/gnome-vfs-init.h>
#include <libgnomevfs/gnome-vfs-utils.h>
#include <libgnomeui/gnome-icon-lookup.h>
#include <libgnomeui/gnome-thumbnail.h>
#include "thumbnailer.hh"

namespace {

// Base code copied from tracker-search-tool.
static GdkPixbuf*
scale_thumbnail(GdkPixbuf* thumbnail_pixbuf, int icon_size)
{
    GdkPixbuf* pixbuf = NULL;
    gfloat scale_factor_x = 1.0;
    gfloat scale_factor_y = 1.0;
    gint scale_x;
    gint scale_y;

    if (gdk_pixbuf_get_width (thumbnail_pixbuf) > icon_size) {
        scale_factor_x = (gfloat) icon_size /
            (gfloat) gdk_pixbuf_get_width (thumbnail_pixbuf);
    }

    if (gdk_pixbuf_get_height (thumbnail_pixbuf) > icon_size) {
        scale_factor_y = (gfloat) icon_size /
            (gfloat) gdk_pixbuf_get_height (thumbnail_pixbuf);
    }

    if (gdk_pixbuf_get_width (thumbnail_pixbuf) >
        gdk_pixbuf_get_height (thumbnail_pixbuf)) {

        scale_x = icon_size;
        scale_y = (gint) (gdk_pixbuf_get_height (thumbnail_pixbuf) *
                          scale_factor_x);
    }
    else {
        scale_x = (gint) (gdk_pixbuf_get_width (thumbnail_pixbuf) *
                          scale_factor_y);
        scale_y = icon_size;
    }

    pixbuf = gdk_pixbuf_scale_simple (thumbnail_pixbuf,
                                      scale_x, scale_y,
                                      GDK_INTERP_BILINEAR);

    return pixbuf;
}

static GdkPixbuf*
get_gnome_thumbnail_image (GnomeThumbnailFactory* gnome_factory,
                           const gchar* file,
                           gint icon_size)
{
    GdkPixbuf* pixbuf = NULL;
    gchar* thumbnail_path;
    gchar* uri;

    uri = gnome_vfs_get_uri_from_local_path (file);
    thumbnail_path = gnome_thumbnail_path_for_uri (uri,
                                                   GNOME_THUMBNAIL_SIZE_NORMAL);

    if (thumbnail_path != NULL) {
        if (g_file_test (thumbnail_path, G_FILE_TEST_EXISTS)) {

            GdkPixbuf* thumbnail_pixbuf = NULL;

            thumbnail_pixbuf = gdk_pixbuf_new_from_file (thumbnail_path, NULL);
            pixbuf = scale_thumbnail(thumbnail_pixbuf, icon_size);
            g_object_unref (thumbnail_pixbuf);
        }
        else {
            char* icon_name = 0;
            GnomeIconLookupFlags lookup_flags = (GnomeIconLookupFlags)
                (GNOME_ICON_LOOKUP_FLAGS_SHOW_SMALL_IMAGES_AS_THEMSELVES |
                 GNOME_ICON_LOOKUP_FLAGS_ALLOW_SVG_AS_THEMSELVES);

            icon_name =
                gnome_icon_lookup_sync (gtk_icon_theme_get_default (),
                                        gnome_factory,
                                        uri,
                                        NULL,
                                        lookup_flags,
                                        NULL);

            pixbuf =
                gtk_icon_theme_load_icon (gtk_icon_theme_get_default (),
                                          icon_name,
                                          icon_size,
                                          GTK_ICON_LOOKUP_FORCE_SVG,
                                          NULL);

            g_free(icon_name);
        }

        g_free (thumbnail_path);
    }

    g_free (uri);

    return pixbuf;
}

} // anonymous namespace

namespace paperbox {

    using std::map;
    using std::queue;
    using std::tr1::shared_ptr;

    /* ThumbnailPixbufData */

    class ThumbnailPixbufData
    {
    public:
        ThumbnailPixbufData() {}
        ~ThumbnailPixbufData() {}

        Glib::RefPtr<Gdk::Pixbuf> thumb_large;
        Glib::RefPtr<Gdk::Pixbuf> thumb_small;
    };

    /* ThumbnailRequest */

    class ThumbnailRequest
    {
    public:
        explicit ThumbnailRequest(const Glib::ustring& a_uri,
                                  ThumbnailSize a_size,
                                  const Thumbnailer::SlotThumbnailReady& a_slot)
            : uri(a_uri),
              size(a_size),
              slot(a_slot)
            {}

        ~ThumbnailRequest() {}

        const char* get_uri() { return uri.c_str(); }

        Glib::ustring      uri;
        ThumbnailSize      size;
        Thumbnailer::SlotThumbnailReady slot;
    };

    /* Thumbnailer::Private */

    class Thumbnailer::Private
    {
    public:
        typedef map<std::string, shared_ptr<ThumbnailPixbufData> >::iterator
        PixbufDataIter;

        explicit Private();
        ~Private();

        void connect_idle_handler();

        void new_request(const Glib::ustring& uri,
                         ThumbnailSize size,
                         const SlotThumbnailReady& slot);

        bool on_idle();

        void invoke_slot(const SlotThumbnailReady& slot,
                         shared_ptr<ThumbnailPixbufData>& pix_data,
                         ThumbnailSize size);

        GnomeThumbnailFactory* gnome_factory;
        map<std::string, shared_ptr<ThumbnailPixbufData>, std::less<std::string> > thumbnails;
        queue<shared_ptr<ThumbnailRequest> > requests;
    };

    Thumbnailer::Private::Private()
    {
        gnome_factory =
            gnome_thumbnail_factory_new (GNOME_THUMBNAIL_SIZE_NORMAL);

        gnome_vfs_init (); // for gnome_icon_lookup_sync
    }

    Thumbnailer::Private::~Private()
    {
        g_object_unref (gnome_factory);
        thumbnails.clear();
    }

    void
    Thumbnailer::Private::connect_idle_handler()
    {
        Glib::signal_idle().connect(
            sigc::mem_fun(*this, &Thumbnailer::Private::on_idle));
    }

    void
    Thumbnailer::Private::new_request(const Glib::ustring& uri,
                                      ThumbnailSize size,
                                      const SlotThumbnailReady& slot)
    {
        shared_ptr<ThumbnailRequest> req(
            new ThumbnailRequest(uri, size, slot));

        if (requests.empty()) connect_idle_handler();

        requests.push(req);
    }

    void
    Thumbnailer::Private::invoke_slot(const SlotThumbnailReady& slot,
                                      shared_ptr<ThumbnailPixbufData>& pix_data,
                                      ThumbnailSize size)
    {
        switch (size) {
        case THUMBNAIL_SIZE_LARGE:
            slot(pix_data->thumb_large);
            break;
        case THUMBNAIL_SIZE_SMALL:
        default:
            slot(pix_data->thumb_small);
            break;
        }
    }

    bool
    Thumbnailer::Private::on_idle()
    {
        if (requests.empty()) {
            // Disconnect the idle handler. Needs to be reconnected on
            // the first new request.
            return false;
        }

        shared_ptr<ThumbnailRequest> req = requests.front();

        GdkPixbuf* cpixbuf_large = NULL;
        GdkPixbuf* cpixbuf_small = NULL;

        cpixbuf_large =
            get_gnome_thumbnail_image (gnome_factory,
                                       req->get_uri(),
                                       static_cast<gint>(THUMBNAIL_SIZE_LARGE));

        cpixbuf_small =
            get_gnome_thumbnail_image (gnome_factory,
                                       req->get_uri(),
                                       static_cast<gint>(THUMBNAIL_SIZE_SMALL));

        shared_ptr<ThumbnailPixbufData> pix_data(new ThumbnailPixbufData());
        pix_data->thumb_large = Glib::wrap(cpixbuf_large);
        pix_data->thumb_small = Glib::wrap(cpixbuf_small);
        thumbnails[req->uri.raw()] = pix_data;

        invoke_slot(req->slot, pix_data, req->size);

        requests.pop();

        return true;
    }

    /* Thumbnailer */

    Thumbnailer::Thumbnailer()
    {
        priv_.reset(new Private());
    }

    Thumbnailer::~Thumbnailer()
    {
    }

    void
    Thumbnailer::request_thumbnail(const Glib::ustring& uri,
                                   ThumbnailSize size,
                                   const SlotThumbnailReady& slot)
    {
        Private::PixbufDataIter iter(priv_->thumbnails.find(uri.raw()));

        if (iter != priv_->thumbnails.end()) {
            priv_->invoke_slot(slot, iter->second, size);
        }
        else {
            priv_->new_request(uri, size, slot);
        }
    }

} // namespace paperbox
