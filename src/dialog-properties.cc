/*
 *  Paperbox - dialog-properties.cc
 *
 *  Copyright (C) 2008 Marko Anastasov
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include <vector>
#include <glibmm-utils/ustring.h>
#include "dialog-properties.hh"
#include "document.hh"
#include "paths.hh"

namespace paperbox {

    DialogProperties::DialogProperties(GtkDialog* cobject,
            const Glib::RefPtr<Gnome::Glade::Xml>& glade)
        :
        Gtk::Dialog(cobject),
        glade_(glade),
        label_location_(0),
        label_title_(0),
        label_author_(0),
        label_pages_(0),
        label_tags_(0)
    {
        glade_->get_widget("label_location_caption", label_location_);
        g_assert(label_location_);

        glade_->get_widget("label_title_caption", label_title_);
        g_assert(label_title_);

        glade_->get_widget("label_author_caption", label_author_);
        g_assert(label_author_);

        glade_->get_widget("label_pages_caption", label_pages_);
        g_assert(label_pages_);

        glade_->get_widget("label_tags_caption", label_tags_);
        g_assert(label_tags_);
    }

    DialogProperties*
    DialogProperties::create()
    {
        Glib::RefPtr<Gnome::Glade::Xml> glade_xml =
            Gnome::Glade::Xml::create(glade_dialog_properties);

        DialogProperties* p = 0;
        glade_xml->get_widget_derived("dialog_properties", p);
        return p;
    }

    int
    DialogProperties::run(std::tr1::shared_ptr<Document>& doc)
    {
        if (! doc->get_subject().empty()) {
            set_title(doc->get_subject());
        } else {
            set_title(doc->get_file_name());
        }

        label_location_->set_text(doc->get_uri());
        label_title_->set_text(doc->get_subject());
        label_author_->set_text(doc->get_author());
        label_pages_->set_text(
                Glib::Util::stringify<int>(doc->get_page_count()));
        
        using std::vector;
        using Glib::ustring;

        vector<ustring> tags = doc->get_tags();
        ustring str;

        vector<ustring>::iterator it(tags.begin());
        vector<ustring>::iterator end(tags.end());
        for ( ; it != end; ++it) {
            str += *it;
            str += ";";
        }

        label_tags_->set_text(str);

        return Gtk::Dialog::run();
    }

} // namespace paperbox
