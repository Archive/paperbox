// -*- Mode: C++; indent-tabs-mode: nil; c-basic-offset: 4 -*-

/*
 *  PaperBox - dialog-tag-entry.cc
 *
 *  Copyright (C) 2007 Marko Anastasov
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include <list>
#include <gdk/gdkkeysyms.h>
#include <glibmm-utils/ustring.h>
#include "browser.hh"
#include "dialog-tag-entry.hh"
#include "entry-multi-completion.h"
#include "paths.hh"

namespace paperbox {

    using std::list;
    using std::vector;
    using Glib::ustring;

    DialogTagEntry::DialogTagEntry(GtkDialog* cobject,
                                   const Glib::RefPtr<Gnome::Glade::Xml>& glade)
        :
        Gtk::Dialog(cobject),
        glade_(glade),
        label_instructions_(0)
    {
        glade_->get_widget("entry_tags", entry_tags_);
        g_assert(entry_tags_);

        Browser* b = Browser::instance();
        list<ustring> tag_suggestions;
        b->get_all_tags(tag_suggestions);

        Glib::RefPtr<Gtk::Util::EntryMultiCompletion> completion =
            Gtk::Util::EntryMultiCompletion::create(tag_suggestions);
        entry_tags_->set_completion(completion);
    }

    DialogTagEntry::~DialogTagEntry()
    {
    }

    DialogTagEntry*
    DialogTagEntry::create()
    {
        Glib::RefPtr<Gnome::Glade::Xml> glade_xml =
            Gnome::Glade::Xml::create(glade_dialog_tag_entry);

        DialogTagEntry* p = 0;
        glade_xml->get_widget_derived("dialog_tag_entry", p);
        return p;
    }

    void
    DialogTagEntry::set_instructions(const Glib::ustring& msg)
    {
        if (! label_instructions_) {
            glade_->get_widget("label_instructions", label_instructions_);
            g_assert(label_instructions_);
        }

        label_instructions_->set_text(msg);
    }

    int
    DialogTagEntry::run(vector<Glib::ustring>& tags)
    {
        int response = Gtk::Dialog::run();

        Glib::ustring tag_input = entry_tags_->get_text();
        Glib::Util::trim(tag_input);

        if (! tag_input.empty()) {
            tags = Glib::Util::split(tag_input, " ");
        }

        return response;
    }

} // namespace paperbox
