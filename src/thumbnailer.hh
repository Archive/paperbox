/*
 *  Paperbox - thumbnailer.hh
 *
 *  Copyright (C) 2007 Marko Anastasov
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#ifndef __PAPERBOX_THUMBNAILER__
#define __PAPERBOX_THUMBNAILER__

#include <tr1/memory>
#include <gdkmm/pixbuf.h>

namespace paperbox {

    enum ThumbnailSize
    {
        THUMBNAIL_SIZE_SMALL = 72,
        THUMBNAIL_SIZE_LARGE = 128
    };

    class Thumbnailer
    {
    public:
        typedef sigc::slot<void, const Glib::RefPtr<Gdk::Pixbuf>& >
        SlotThumbnailReady;

        explicit Thumbnailer();
        ~Thumbnailer();

        void request_thumbnail(const Glib::ustring& uri,
                               ThumbnailSize size,
                               const SlotThumbnailReady& slot);

    protected:
        struct Private;
        std::tr1::shared_ptr<Private> priv_;
    };

} // namespace paperbox

#endif // __PAPERBOX_THUMBNAILER__
