// -*- Mode: C++; indent-tabs-mode: nil; c-basic-offset: 4 -*-

/*
 *  PaperBox - tag-request.hh
 *
 *  Copyright (C) 2007 Marko Anastasov
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#ifndef __PAPER_BOX_TAG_REQUEST__
#define __PAPER_BOX_TAG_REQUEST__

#include <vector>
#include <glibmm/ustring.h>
#include <tracker.h>

namespace paperbox {

    class TagRequest
    {
    public:
        explicit TagRequest(TrackerClient* tracker_client,
                            const Glib::ustring& uri);

        ~TagRequest();

        void get_results(std::vector<Glib::ustring>& tag_receiver);

    protected:
        TrackerClient* client_;
        Glib::ustring  uri_;
    };

} // namespace paperbox

#endif // __PAPER_BOX_TAG_REQUEST__
