/*
 *  Paperbox - document-tile.cc
 *
 *  Copyright (C) 2007-2008 Marko Anastasov
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include <glib/gi18n.h>
#include <gtk/gtklinkbutton.h>
#include <gtk/gtkwidget.h>
#include <glibmm-utils/ustring.h>
#include <gtkmm/stock.h>
#include "browser.hh"
#include "dialog-tag-entry.hh"
#include "document-tile.hh"

namespace paperbox {

    using std::string;
    using std::vector;
    using std::tr1::shared_ptr;
    using Glib::ustring;

    const unsigned int TITLE_BREAK = 50;

    DocumentTile::DocumentTile(Thumbnailer& thumbnailer,
                               const shared_ptr<Document>& doc)
        :
        Gtk::Util::Tile(doc->get_subject(), doc->get_author(), false),
        uri_(doc->get_uri()),
        doc_(doc),
        thumbnailer_(&thumbnailer),
        tag_hbox_(false, 4),
        tag_labels_hbox_(false, 4),
        tag_links_hbox_(false, 4),
        tag_tools_hbox_(false, 4),
        tags_caption_(_("Tags:")),
        image_add_(Gtk::Stock::ADD, Gtk::ICON_SIZE_MENU),
        first_boot_(true)
    {
        break_title();

        Gtk::VBox& content_vbox = get_content_vbox();

        pages_label_.set_alignment(0.0);
        extra_hbox_.pack_start(pages_label_, false, false, 0);

        modtime_label_.set_alignment(0.0);
        time_hbox_.pack_start(modtime_label_);

        // Build the tag bar
        tag_tools_hbox_.pack_start(tags_caption_, false, false, 0);

        button_add_tags_.set_relief(Gtk::RELIEF_NONE);
        button_add_tags_.set_focus_on_click(false);
        button_add_tags_.add(image_add_);
        tag_tools_hbox_.pack_start(button_add_tags_, false, false, 0);

        tag_hbox_.pack_start(tag_labels_hbox_, false, false, 0);

        content_vbox.pack_start(tag_hbox_, false, false, 0);

        connect_signals();
    }

    DocumentTile::~DocumentTile()
    {
        tag_links_.clear();
        tag_labels_.clear();
    }

    ustring
    DocumentTile::get_document_uri() const
    {
        return uri_;
    }

    std::string
    DocumentTile::get_document_uri_raw() const
    {
        return uri_.raw();
    }

    void
    DocumentTile::connect_thumbnailer_signals(Thumbnailer* thumbnailer)
    {
        thumbnailer->request_thumbnail(
            uri_, 
            THUMBNAIL_SIZE_SMALL,
            sigc::mem_fun(*this, &DocumentTile::on_small_thumbnail_ready));

        thumbnailer->request_thumbnail(
            uri_, 
            THUMBNAIL_SIZE_LARGE,
            sigc::mem_fun(*this, &DocumentTile::on_large_thumbnail_ready));
    }

    void
    DocumentTile::connect_signals()
    {
        Gtk::LinkButton::set_uri_hook(
            sigc::mem_fun(*this, &DocumentTile::on_taglink));

        button_add_tags_.signal_clicked().connect(
            sigc::mem_fun(*this, &DocumentTile::on_tag_add_clicked));

        Browser* b = Browser::instance();

        b->signal_document_retrieved().connect(
            sigc::mem_fun(*this, &DocumentTile::on_document_retrieved));

        b->signal_tags_changed().connect(
            sigc::mem_fun(*this, &DocumentTile::on_tags_changed));
    }

    void
    DocumentTile::on_document_retrieved(const shared_ptr<Document>& doc)
    {
        if (doc_->get_uri() != doc->get_uri()) return;

        set_title(doc->get_subject());
        set_summary(doc->get_author());
        break_title();

        int page_count = doc->get_page_count();

        pages_label_.set_text(
            Glib::Util::uprintf(ngettext("%d page", "%d pages", page_count),
                                page_count));

        modtime_label_.set_text(
            Glib::Util::uprintf(_("Modified on %s"),
                                doc->get_modification_time().c_str()));

        // Build tag links from tag string
        vector<ustring> tags = doc->get_tags();
        vector<ustring> removed(0); // none

        refresh_tag_links(tags, removed);
        pack_tag_links();

        refresh_tag_labels(tags, removed);
        pack_tag_labels();
    }

    Gtk::Widget*
    DocumentTile::get_tag_link(const string& tag)
    {
        TagLinkButton* widget = 0;
        tag_link_map::iterator link_iter = tag_links_.find(tag);

        if (link_iter != tag_links_.end()) {
            widget = static_cast<TagLinkButton*>(link_iter->second.get());
        }
        else {
            widget = new TagLinkButton(tag);

            widget->signal_tag_remove_request().connect(
                sigc::mem_fun(*this,
                              &DocumentTile::on_tag_link_tag_remove_request));

            shared_ptr<Gtk::Widget> shared_widget(widget);
            tag_links_[tag] = shared_widget;
        }

        return dynamic_cast<Gtk::Widget*>(widget);
    }

    Gtk::Widget*
    DocumentTile::get_tag_label(const string& tag)
    {
        Gtk::Label* widget = 0;
        tag_label_map::iterator label_iter = tag_labels_.find(tag);

        if (label_iter != tag_labels_.end()) {
            widget = static_cast<Gtk::Label*>(label_iter->second.get());
        } else {
            widget = new Gtk::Label(tag);

            // lighten up the colour a bit
            GdkColor color;
            gdk_color_parse ("LightSlateGray", &color);
            gtk_widget_modify_fg (GTK_WIDGET(widget->gobj()),
                                  GTK_STATE_NORMAL,
                                  &color);
            
            shared_ptr<Gtk::Widget> shared_widget(widget);
            tag_labels_[tag] = shared_widget;
        }

        return dynamic_cast<Gtk::Widget*>(widget);
    }

    void
    DocumentTile::refresh_tag_links(const vector<ustring>& tags_added,
                                    const vector<ustring>& tags_removed)
    {
        {
            vector<ustring>::const_iterator it(tags_added.begin());
            vector<ustring>::const_iterator end(tags_added.end());

            for ( ; it != end; ++it)
                get_tag_link(it->raw()); // create or do nothing
        }

        {
            vector<ustring>::const_iterator it(tags_removed.begin());
            vector<ustring>::const_iterator end(tags_removed.end());

            for ( ; it != end; ++it) {
                tag_link_map::iterator link_it = tag_links_.find(it->raw());
                if (link_it != tag_links_.end())
                    tag_links_.erase(link_it);
            }
        }
    }
    
    void
    DocumentTile::refresh_tag_labels(const vector<ustring>& tags_added,
                                     const vector<ustring>& tags_removed)
    {
        {
            vector<ustring>::const_iterator it(tags_added.begin());
            vector<ustring>::const_iterator end(tags_added.end());

            for ( ; it != end; ++it)
                get_tag_label(it->raw()); // create or do nothing
        }

        {
            vector<ustring>::const_iterator it(tags_removed.begin());
            vector<ustring>::const_iterator end(tags_removed.end());

            for ( ; it != end; ++it) {
                tag_label_map::iterator label_it = tag_labels_.find(it->raw());
                if (label_it != tag_labels_.end())
                    tag_labels_.erase(label_it);
            }
        }
    }

    void
    DocumentTile::pack_tag_links()
    {
        tag_link_map::iterator it(tag_links_.begin());
        tag_link_map::iterator end(tag_links_.end());

        for ( ; it != end; ++it) {
            Gtk::Widget* link_widget = it->second.get();
            link_widget->show();
            tag_links_hbox_.pack_start(*link_widget, false, false, 0);
        }
    }

    void
    DocumentTile::unpack_tag_links()
    {
        tag_link_map::iterator it(tag_links_.begin());
        tag_link_map::iterator end(tag_links_.end());

        for ( ; it != end; ++it) {
            TagLinkButton* button =
                static_cast<TagLinkButton*>(it->second.get());

            tag_links_hbox_.remove(*button);
        }
    }
    
    void
    DocumentTile::pack_tag_labels()
    {
        tag_label_map::iterator it(tag_labels_.begin());
        tag_label_map::iterator end(tag_labels_.end());

        for ( ; it != end; ++it) {
            Gtk::Widget* label = it->second.get();
            label->show();
            tag_labels_hbox_.pack_start(*label, false, false, 0);
        }
    }

    void
    DocumentTile::unpack_tag_labels()
    {
        tag_label_map::iterator it(tag_labels_.begin());
        tag_label_map::iterator end(tag_labels_.end());

        for ( ; it != end; ++it) {
            Gtk::Label* label = static_cast<Gtk::Label*>(it->second.get());
            tag_labels_hbox_.remove(*label);
        }
    }

    DocumentTile::SignalTagClicked&
    DocumentTile::signal_tag_clicked()
    {
        return signal_tag_clicked_;
    }

    void
    DocumentTile::on_show()
    {
        Tile::on_show();
        if (! thumbnail_small_) connect_thumbnailer_signals(thumbnailer_);
    }

    void
    DocumentTile::on_selected()
    {
        Gtk::VBox& content_vbox = get_content_vbox();

        if (first_boot_) {
            content_vbox.pack_start(extra_hbox_, false, false);
            content_vbox.pack_start(time_hbox_, false, false);

            tag_hbox_.pack_start(tag_links_hbox_, false, false);
            tag_hbox_.pack_start(tag_tools_hbox_, false, false);

            //content_vbox.show_all();
            first_boot_ = false;
        }

        tag_links_hbox_.show_all();

        content_vbox.reorder_child(extra_hbox_, 2);
        content_vbox.reorder_child(time_hbox_, 3);
        tag_hbox_.reorder_child(tag_tools_hbox_, 0);

        extra_hbox_.show_all();
        time_hbox_.show_all();
        tag_tools_hbox_.show_all();

        tag_labels_hbox_.hide();

        get_image().set(thumbnail_large_);
        restore_title();
    }

    void
    DocumentTile::on_unselected()
    {
        extra_hbox_.hide();
        time_hbox_.hide();
        tag_tools_hbox_.hide();

        tag_labels_hbox_.show_all();
        tag_links_hbox_.hide();

        get_image().set(thumbnail_small_);
        break_title();
    }

    void
    DocumentTile::on_large_thumbnail_ready(
        const Glib::RefPtr<Gdk::Pixbuf>& pixbuf)
    {
        thumbnail_large_ = pixbuf;
    }

    void
    DocumentTile::on_small_thumbnail_ready(
        const Glib::RefPtr<Gdk::Pixbuf>& pixbuf)
    {
        thumbnail_small_ = pixbuf;

        Gtk::Image& tile_image = get_image();
        tile_image.set(thumbnail_small_);
    }

    void
    DocumentTile::on_tag_add_clicked()
    {
        vector<ustring> tags;
        shared_ptr<DialogTagEntry> dialog(DialogTagEntry::create());
        dialog->set_default_response(Gtk::RESPONSE_OK);
        if (! doc_->get_subject().empty()) {
            dialog->set_title(doc_->get_subject());
        } else {
            dialog->set_title(doc_->get_file_name());
        }

        int response = dialog->run(tags);

        if (response == Gtk::RESPONSE_OK) {
            Browser* b = Browser::instance();
            b->add_tags(uri_, tags);
        }
    }

    void
    DocumentTile::on_taglink(Gtk::LinkButton* /* button */,
                             const ustring& uri)
    {
        signal_tag_clicked_.emit(uri);
    }

    void
    DocumentTile::on_tag_link_tag_remove_request(const ustring& tag)
    {
        vector<ustring> tag_vec;
        tag_vec.push_back(tag);
        Browser* b = Browser::instance();
        b->remove_tags(uri_, tag_vec);
    }

    void
    DocumentTile::on_tags_changed(const std::string& uri,
                                  const vector<ustring>& tags_added,
                                  const vector<ustring>& tags_removed)
    {
        if (uri_ != uri) return;

        unpack_tag_links();
        refresh_tag_links(tags_added, tags_removed);
        pack_tag_links();

        unpack_tag_labels();
        refresh_tag_labels(tags_added, tags_removed);
        pack_tag_labels();
    }

    void
    DocumentTile::hide_extra_info()
    {
        on_unselected();
    }

    void
    DocumentTile::break_title()
    {
        Gtk::Label& title_label = get_title_label();
        title_label.set_ellipsize(Pango::ELLIPSIZE_END);
        title_label.set_max_width_chars(TITLE_BREAK);

        if (! doc_->get_subject().empty()) {
            set_title(doc_->get_subject());
        } else {
            set_title(doc_->get_file_name());
        }
    }

    void
    DocumentTile::restore_title()
    {
        Gtk::Label& title_label = get_title_label();
        ustring title = get_title();

        title_label.set_max_width_chars(80); // something large enough
        if (title.size() > TITLE_BREAK) {
            title.insert(TITLE_BREAK, "\n");
            set_title(title);
        }
    }

    shared_ptr<Document>
    DocumentTile::get_document()
    {
        return doc_;
    }

    void
    DocumentTile::show_add_tag_dialog()
    {
        on_tag_add_clicked();
    }

} // namespace paperbox
