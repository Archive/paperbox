/*
 *  Paperbox - browser.cc
 *
 *  Copyright (C) 2007 Marko Anastasov
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include <algorithm>
#include <iostream>
#include <glibmm/fileutils.h>
#include <glibmm/main.h>
#include <glibmm-utils/log-stream-utils.h>
#include "browser.hh"

namespace paperbox {

    using std::auto_ptr;
    using std::list;
    using std::vector;
    using std::tr1::shared_ptr;
    using Glib::ustring;

    ///

    int
    cmp_nocase(const ustring& s, const ustring& s2)
    {
        ustring::const_iterator p = s.begin();
        ustring::const_iterator p2 = s2.begin();

        while (p != s.end() && p2 != s2.end()) {
            if (toupper(*p) != toupper(*p2)) {
                return (toupper(*p) < toupper(*p2)) ? -1 : 1;
            }
            ++p;
            ++p2;
        }

        return (s2.size() == s.size()) ? 0 : (s.size() < s2.size()) ? -1 : 1;
    }

    struct doc_az_compare
        : public std::binary_function<shared_ptr<Document>,
                                      shared_ptr<Document>,
                                      bool>
    {
        bool
        operator()(const shared_ptr<Document>& lhs,
                   const shared_ptr<Document>& rhs) const
            {
                Glib::ustring title1, title2;

                if (! lhs->get_subject().empty()) {
                    title1 = lhs->get_subject();
                } else {
                    title1 = lhs->get_file_name();
                }

                if (! rhs->get_subject().empty()) {
                    title2 = rhs->get_subject();
                } else {
                    title2 = rhs->get_file_name();
                }

                return cmp_nocase(title1, title2) == -1;
            }
    };

    struct doc_mtime_compare_asc
        : public std::binary_function<shared_ptr<Document>,
                                      shared_ptr<Document>,
                                      bool>
    {
        bool
        operator()(const shared_ptr<Document>& lhs,
                   const shared_ptr<Document>& rhs) const
            {
                return (lhs->get_mtime() < rhs->get_mtime());
            }
    };

    struct doc_mtime_compare_desc
        : public std::binary_function<shared_ptr<Document>,
                                      shared_ptr<Document>,
                                      bool>
    {
        bool
        operator()(const shared_ptr<Document>& lhs,
                   const shared_ptr<Document>& rhs) const
            {
                return (lhs->get_mtime() > rhs->get_mtime());
            }
    };

    ///

    auto_ptr<Browser> Browser::instance_;

    Browser*
    Browser::instance()
    {
        if (! instance_.get())
            instance_.reset(new Browser());

        return instance_.get();
    }

    Browser::Browser()
    {
        tracker_client_.reset(new TrackerPhone()); // let exception propagate

        connect_to_tracker_signals();
        tracker_client_->request_all_documents();
    }

    Browser::~Browser()
    {
    }

    void
    Browser::connect_to_tracker_signals()
    {
        tracker_client_->signal_documents().connect(
            sigc::mem_fun(*this, &Browser::on_documents));

        tracker_client_->signal_tags_added().connect(
            sigc::mem_fun(*this, &Browser::on_tags_added));

        tracker_client_->signal_tags_removed().connect(
            sigc::mem_fun(*this, &Browser::on_tags_removed));
    }

    void
    Browser::on_documents(const std::queue<ustring>& uris)
    {
        signal_retrieval_started_.emit();

        vector<shared_ptr<Document> > docs;
        std::queue<ustring> uris_copy(uris);

        while (! uris_copy.empty()) {
            ustring uri = uris_copy.front();
            shared_ptr<Document> doc (new Document(uri));
            docs.push_back(doc);
            uri_queue_.push(doc);
            uris_copy.pop();
        }

        signal_new_blank_documents_.emit(docs);

        Glib::signal_idle().connect(
            sigc::mem_fun(*this, &Browser::on_idle_initial_document_retrieval));
    }

    bool
    Browser::on_idle_initial_document_retrieval()
    {
        if (uri_queue_.empty()) {
            // The queue being empty at the beginning and not after a pop()
            // means that  either there is nothing on the desktop,
            // or trackerd is (re)indexing and being unresponsive.
            LOG_DD("Cancelling document retrieval from tracker - "
                   << "is it (re)indexing? "
                   << "Try running Paperbox again after a minute.");
            signal_retrieval_finished_.emit();
            return false; // idle function done
        }

        shared_ptr<Document> doc = uri_queue_.front();
        ustring uri = doc->get_uri();

        // get metadata, emit for new documents
        try {
            tracker_client_->get_document(uri, doc);

            docs_[uri.raw()] = doc;
            signal_document_retrieved_.emit(doc);
        }
        catch (const Glib::FileError& ex) {
            // probably a nonexistent file in the index
            LOG_EXCEPTION(uri << ": " << ex.what());
        } catch (const std::exception& ex) {
            LOG_EXCEPTION("Unexpected exception while retrieving document "
                    << uri << ": " << ex.what());
        }

        uri_queue_.pop();

        if (uri_queue_.empty()) {
            signal_retrieval_finished_.emit();
            return false; // done with all
        } else {
            return true; // continue working in idle time
        }
    }

    sigc::signal<void>&
    Browser::signal_retrieval_started()
    {
        return signal_retrieval_started_;
    }

    sigc::signal<void>&
    Browser::signal_retrieval_finished()
    {
        return signal_retrieval_finished_;
    }

    Browser::SignalDocuments&
    Browser::signal_new_blank_documents()
    {
        return signal_new_blank_documents_;
    }

    Browser::SignalDocument&
    Browser::signal_document_retrieved()
    {
        return signal_document_retrieved_;
    }

    Browser::SignalTagsChanged&
    Browser::signal_tags_changed()
    {
        return signal_tags_changed_;
    }

    void
    Browser::on_tags_added(const ustring& uri,
                           const vector<ustring>& tags)
    {
        doc_map::iterator doc_iter = docs_.find(uri.raw());
        shared_ptr<Document> doc = doc_iter->second;
        // Ignore if a mime type which not in our index has been tagged
        if (! doc.get()) return;

        vector<ustring>::const_iterator it(tags.begin());
        vector<ustring>::const_iterator end(tags.end());

        for ( ; it != end; ++it) doc->add_tag(*it);

        vector<ustring> tags_removed_none;
        signal_tags_changed_.emit(uri, tags, tags_removed_none);
    }

    // Does the validation work and forwards the add request to TrackerPhone
    void
    Browser::add_tags(const ustring& uri, const vector<ustring>& tags)
    {
        doc_map::iterator doc_iter = docs_.find(uri.raw());

        if (doc_iter == docs_.end() || tags.empty()) return;

        // only include the tags that do not intersect with the existing
        shared_ptr<Document> doc = doc_iter->second;
        vector<ustring> existing_tags = doc->get_tags();
        vector<ustring> tags_copy(tags);

        vector<ustring>::iterator it(existing_tags.begin());
        vector<ustring>::iterator end(existing_tags.end());
        for ( ; it != end; ++it) {
            tags_copy.erase(
                std::remove(tags_copy.begin(), tags_copy.end(), *it),
                tags_copy.end());
        }

        if (tags_copy.empty()) return;

        tracker_client_->add_tags(uri, tags_copy);
    }

    void
    Browser::on_tags_removed(const ustring& uri,
                             const vector<ustring>& tags)
    {
        doc_map::iterator doc_iter = docs_.find(uri.raw());
        shared_ptr<Document> doc = doc_iter->second;
        // Ignore if a mime type which not in our index has been un-tagged
        if (! doc.get()) return;

        vector<ustring>::const_iterator it(tags.begin());
        vector<ustring>::const_iterator end(tags.end());

        for ( ; it != end; ++it) doc->remove_tag(*it);

        vector<ustring> tags_added_none;
        signal_tags_changed_.emit(uri, tags_added_none, tags);
    }

    // Does the validation work and forwards the remove request to TrackerPhone
    void
    Browser::remove_tags(const ustring& uri,
                         const std::vector<ustring>& tags)
    {
        doc_map::iterator doc_iter = docs_.find(uri.raw());

        if (doc_iter == docs_.end() || tags.empty()) return;

        // only include the tags that intersect with the existing
        shared_ptr<Document> doc = doc_iter->second;
        vector<ustring> existing_tags = doc->get_tags();
        vector<ustring> tags_copy(tags);

        vector<ustring>::iterator it(tags_copy.begin());
        vector<ustring>::iterator end(tags_copy.end());
        for ( ; it != end; ++it) {
            if (find(existing_tags.begin(), existing_tags.end(), *it) ==
                existing_tags.end()) {
                // invalid, not found
                tags_copy.erase(
                    std::remove(tags_copy.begin(), tags_copy.end(), *it),
                    tags_copy.end());
            }
        }

        if (tags_copy.empty()) return;

        tracker_client_->remove_tags(uri, tags_copy);
    }

    void
    Browser::rename_tag(const ustring& /*uri*/,
                        const ustring& /*from_tag*/,
                        const ustring& /*to_tag*/)
    {
        //TODO later as a high-level functionality, in UI it should
        // not be strictly bound to one document. It should rather be
        // done in some kind of a manage-tags dialog.
        g_debug("rename_tag not implemented yet");
    }

    void
    Browser::get_all_tags(list<ustring>& tags)
    {
        doc_map::iterator it(docs_.begin());
        doc_map::iterator end(docs_.end());

        for ( ; it != end; ++it) {
            shared_ptr<Document> doc = it->second;
            vector<ustring> doc_tags = doc->get_tags();

            vector<ustring>::iterator tit(doc_tags.begin());
            vector<ustring>::iterator tend(doc_tags.end());

            for ( ; tit != tend; ++tit) {
                list<ustring>::iterator result =
                    find(tags.begin(), tags.end(), *tit);
                if (result == tags.end()) tags.push_back(*tit);
            }
        }

        tags.sort();
    }

    void
    Browser::get_all_documents(doc_vector& docs, DocumentSorting sorting)
    {
        doc_map::iterator it(docs_.begin());
        doc_map::iterator end(docs_.end());

        for ( ; it != end; ++it)
            docs.push_back(it->second);

        sort_documents(docs, sorting);
    }

    void
    Browser::sort_documents(doc_vector& docs, DocumentSorting sorting)
    {
        if (sorting == DOCUMENT_SORTING_ALPHABETICAL) {
            sort(docs.begin(), docs.end(), doc_az_compare());
        } else if (sorting == DOCUMENT_SORTING_BY_DATE_ASC) {
            sort(docs.begin(), docs.end(), doc_mtime_compare_asc());
        } else if (sorting == DOCUMENT_SORTING_BY_DATE_DESC) {
            sort(docs.begin(), docs.end(), doc_mtime_compare_desc());
        }
    }

    void
    Browser::get_untagged_documents(doc_vector& docs)
    {
        doc_vector all_docs;
        get_all_documents(all_docs);

        doc_vector::iterator it(all_docs.begin());
        doc_vector::iterator end(all_docs.end());

        for ( ; it != end; ++it)
            if (! (*it)->get_tags().size())
                docs.push_back(*it);
    }

    void
    Browser::get_documents_for_tag(const ustring& tag,
                                   doc_vector& docs_ret)
    {
        doc_map::iterator it(docs_.begin());
        doc_map::iterator end(docs_.end());

        for ( ; it != end; ++it) {
            shared_ptr<Document> doc = it->second;
            if (doc->contains_tag(tag))
                docs_ret.push_back(doc);
        }
    }

    void
    Browser::get_documents_for_tag_bundle(const vector<ustring>& tags,
                                          doc_vector& docs_ret)
    {
        doc_map::iterator it(docs_.begin());
        doc_map::iterator end(docs_.end());

        for ( ; it != end; ++it) {
            shared_ptr<Document> doc = it->second;

            vector<ustring>::const_iterator tit(tags.begin());
            vector<ustring>::const_iterator tend(tags.end());
            for ( ; tit != tend; ++tit) {
                if (doc->contains_tag(*tit)) {
                    docs_ret.push_back(doc);
                    break;
                }
            }
        }
    }

} // namespace paperbox
