/*
 *  Paperbox - document-tile-view.hh
 *
 *  Copyright (C) 2007-2008 Marko Anastasov
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#ifndef __PAPERBOX_DOCUMENT_TILE_VIEW_HH__
#define __PAPERBOX_DOCUMENT_TILE_VIEW_HH__

#include <vector>
#include <tr1/memory>
#include <gtkmm-utils/tile-view.h>
#include "document-tile.hh"

namespace paperbox {

    class Document;

    class DocumentTileView : public Gtk::Util::TileView
    {
    public:
        typedef sigc::signal<void, const Glib::ustring&> SignalTagClicked;

        explicit DocumentTileView(bool use_page_view = true);
        ~DocumentTileView();

        // TileView overrides
        virtual void add_tile(Gtk::Util::Tile& tile);
        virtual void add_tile(Gtk::Util::Tile* tile);

        virtual void get_displayed_documents(
                std::vector<std::tr1::shared_ptr<Document> >& docs);

        // Signal propagated from individual DocumentTileView widgets
        SignalTagClicked& signal_tag_clicked() { return signal_tag_clicked_; }

    protected:
        // TileView override
        virtual void on_show_request();

        virtual void extra_info_hider(Gtk::Util::Tile& t);

        virtual void connect_to_extra_signals(DocumentTile* tile);

        // Handlers for additional signals that DocumentTile provides.
        virtual void on_tag_selected(const Glib::ustring& tag);

        virtual void tile_walker(Gtk::Util::Tile& tile,
                std::vector<std::tr1::shared_ptr<Document> >* current_docs);

        SignalTagClicked signal_tag_clicked_;
    };

} 

#endif // __PAPERBOX_DOCUMENT_TILE_VIEW_HH__
