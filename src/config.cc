/*
 *  Paperbox - config.cc
 *
 *  Copyright (C) 2008 Marko Anastasov
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include <errno.h>
#include <sys/stat.h>
#include <glibmm/fileutils.h>
#include <glibmm-utils/log-stream-utils.h>
#include <giomm/error.h>
#include <giomm/file.h>
#include "category.hh"
#include "config.hh"
#include "main-window.hh"

namespace paperbox {

    using std::tr1::shared_ptr;
    using Glib::ustring;

    const ustring UI_GROUP = "UI";

    // Will create ~/.config/paperbox and ~/.config/paperbox/categories.
    // Call it from main().
    void
    init_config_dirs()
    {
        std::string path = paperbox::Category::get_default_path();
        
        if (! Glib::file_test(path, Glib::FILE_TEST_EXISTS))
            if (g_mkdir_with_parents(path.c_str(), S_IRWXU) == -1)
                g_warning("Could not make directory %s with parents: %s",
                          path.c_str(),
                          g_strerror(errno));
    }

    // Returns the default implementation of the Config interface to
    // the rest of the application.
    shared_ptr<Config>
    get_default_config()
    {
        shared_ptr<Config> cfg(new KeyFileConfig());
        return cfg;
    }

    KeyFileConfig::KeyFileConfig()
    {
        check_file_presence();

        try {
            if (! keyfile_.load_from_file(KEY_FILE_CONFIG_PATH)) {
                g_warning("Could not find configuration file!");
                //TODO: handle this better, although I don't know
                //if false is even possible since there's exception throwing
                //File a bug or something for glibmm docs.
            }

            if (! has_all_fields()) {
                write_default(true);
                // reload, although this will reset previous configuration,
                // it should be smarter in later versions
                keyfile_.load_from_file(KEY_FILE_CONFIG_PATH);
            }

        } catch (Glib::FileError& e) {
            LOG_EXCEPTION("Could not load program configuration file: "
                          << e.what());
        }
    }

    KeyFileConfig::~KeyFileConfig()
    {
    }

    void
    KeyFileConfig::write_default(bool replace)
    {
        Glib::KeyFile keyfile_;

        keyfile_.set_integer(UI_GROUP, "window_width", DEFAULT_WIDTH);
        keyfile_.set_integer(UI_GROUP, "window_height", DEFAULT_HEIGHT);
        keyfile_.set_integer(UI_GROUP, "main_paned_position",
                             3 * (DEFAULT_WIDTH / 4));
        keyfile_.set_integer(UI_GROUP, "category_paned_position", 
                             DEFAULT_CATEGORY_PANED_POS);

        Glib::RefPtr<Gio::File> file =
            Gio::File::create_for_path(KEY_FILE_CONFIG_PATH);
        Glib::RefPtr<Gio::FileOutputStream> stream;
        stream = replace ? file->replace() : file->create_file();
        stream->write(keyfile_.to_data());
        stream->close();
    }

    // Creates ~/.config/paperbox/config.ini if it does not exist.
    void
    KeyFileConfig::check_file_presence()
    {
        if (! Glib::file_test(KEY_FILE_CONFIG_PATH, Glib::FILE_TEST_EXISTS))
            write_default(false);
    }

    bool
    KeyFileConfig::has_all_fields()
    {
        try {
            return keyfile_.has_key(UI_GROUP, "window_width") &&
                keyfile_.has_key(UI_GROUP, "window_height") &&
                keyfile_.has_key(UI_GROUP, "main_paned_position") &&
                keyfile_.has_key(UI_GROUP, "category_paned_position");
        } catch (const Glib::KeyFileError& e) {
            LOG_EXCEPTION("Could not check configuration keys for presence"
                    << e.what());
            return false;
        }
    }

    void
    KeyFileConfig::get_window_size(int& width, int& height)
    {
        width = keyfile_.get_integer(UI_GROUP, "window_width");
        height = keyfile_.get_integer(UI_GROUP, "window_height");
    }

    void
    KeyFileConfig::get_main_paned_position(int& pos)
    {
        pos = keyfile_.get_integer(UI_GROUP, "main_paned_position");
    }

    void
    KeyFileConfig::set_window_size(const int width, const int height)
    {
        keyfile_.set_integer(UI_GROUP, "window_width", width);
        keyfile_.set_integer(UI_GROUP, "window_height", height);
    }

    void
    KeyFileConfig::set_main_paned_position(const int pos)
    {
        keyfile_.set_integer(UI_GROUP, "main_paned_position", pos);
    }


    void
    KeyFileConfig::get_category_paned_position(int& pos)
    {
        pos = keyfile_.get_integer(UI_GROUP, "category_paned_position");
    }

    void
    KeyFileConfig::set_category_paned_position(const int pos)
    {
        keyfile_.set_integer(UI_GROUP, "category_paned_position", pos);
    }

    void
    KeyFileConfig::save()
    {
        Glib::RefPtr<Gio::File> file =
            Gio::File::create_for_path(KEY_FILE_CONFIG_PATH);

        try {
            Glib::RefPtr<Gio::FileOutputStream> stream = file->replace();
            stream->write(keyfile_.to_data());
            stream->close();
        } catch (Gio::Error& e) {
            LOG_EXCEPTION("Failed to save key file configuration: "
                          << e.what());
        }
    }

}
