/*
 *  Paperbox - category-editor-model.hh
 *
 *  Copyright (C) 2008 Marko Anastasov
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#ifndef __PAPERBOX_CATEGORY_EDITOR_MODEL_HH__
#define __PAPERBOX_CATEGORY_EDITOR_MODEL_HH__

#include <map>
#include <tr1/memory>
#include <gtkmm/textbuffer.h>
#include "category.hh"
#include "category-factory.hh"

namespace paperbox {

    class CategoryEditorData : public NonCopyable
    {
    public:
        explicit CategoryEditorData(const Glib::ustring& name);
        explicit CategoryEditorData(std::tr1::shared_ptr<Category>& cat);
        ~CategoryEditorData() {}

        std::tr1::shared_ptr<Category> category;
        Glib::RefPtr<Gtk::TextBuffer> buffer;
        bool dirty;
    };

    class CategoryEditorModel
    {
    public:
        explicit CategoryEditorModel();
        ~CategoryEditorModel();

        std::list<std::tr1::shared_ptr<CategoryEditorData> > load_category_data();

        // throws CategoryExists, propagated from CategoryFactory
        std::tr1::shared_ptr<CategoryEditorData> new_category(const Glib::ustring& name);

        // check for pointer, might return null
        std::tr1::shared_ptr<CategoryEditorData> get_category(const Glib::ustring& name);

        void save_category(const Glib::ustring& name);

        // throws CategoryNotFound, from CategoryFactory
        void delete_category(const Glib::ustring& name);

    protected:
        std::map<Glib::ustring,
                 std::tr1::shared_ptr<CategoryEditorData> > model_data_;
    };

} // namespace paperbox

#endif // __PAPERBOX_CATEGORY_EDITOR_MODEL_HH__
