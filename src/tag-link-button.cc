// -*- Mode: C++; indent-tabs-mode: nil; c-basic-offset: 4 -*-

/*
 *  PaperBox - tag-link-button.cc
 *
 *  Copyright (C) 2007 Marko Anastasov
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include <glib/gi18n.h>
#include <gtkmm/stock.h>
#include <glibmm-utils/log-stream-utils.h>
#include "tag-link-button.hh"

namespace paperbox {

    using std::vector;

    TagLinkButton::TagLinkButton(const Glib::ustring& tag)
        :
        Gtk::LinkButton(tag, tag),
        // atm there's a bug in gtkmm's when constructing a linkbutton
        // with one parameter
        tag_(tag),
        popup_menu_(0)
    {
        fill_menu();
    }

    TagLinkButton::~TagLinkButton()
    {
    }

    void
    TagLinkButton::fill_menu()
    {
        action_group_ = Gtk::ActionGroup::create();

        action_group_->add(Gtk::Action::create("ContextMenu", "Context Menu"));

        action_group_->add(
            Gtk::Action::create("ContextRemoveTag", Gtk::Stock::REMOVE),
            sigc::mem_fun(*this, &TagLinkButton::on_popup_remove));

        ui_manager_ = Gtk::UIManager::create();
        ui_manager_->insert_action_group(action_group_);

        Glib::ustring ui_info =
            "<ui>"
            "  <popup name='TagLinkButtonPopupMenu'>"
            "    <menuitem action='ContextRemoveTag'/>"
            "  </popup>"
            "</ui>";

        try {
            ui_manager_->add_ui_from_string(ui_info);
        }
        catch(const Glib::Error& ex) {
            LOG_ERROR("building tlb popup menu failed: " << ex.what());
        }

        //Get the menu:
        popup_menu_ = dynamic_cast<Gtk::Menu*>(
            ui_manager_->get_widget("/TagLinkButtonPopupMenu")); 

        if (! popup_menu_)
            LOG_ERROR("tag-link-button's popup menu not found");
    }

    bool
    TagLinkButton::on_button_press_event(GdkEventButton* event)
    {
        if ((event->type == GDK_BUTTON_PRESS) && (event->button == 3)) {
            if (popup_menu_)
                popup_menu_->popup(event->button, event->time);
            return true; // we handled the event
        }
        else {
            return Gtk::LinkButton::on_button_press_event(event);
        }
    }

    void
    TagLinkButton::on_popup_remove()
    {
        signal_tag_remove_request_.emit(tag_);
    }

    TagLinkButton::SignalTagRemoveRequest&
    TagLinkButton::signal_tag_remove_request()
    {
        return signal_tag_remove_request_;
    }

} // namespace paperbox
