// -*- Mode: C++; indent-tabs-mode: nil; c-basic-offset: 4 -*-

/*
 *  PaperBox - document.hh
 *
 *  Copyright (C) 2007 Marko Anastasov
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#ifndef __PAPER_BOX_DOCUMENT__
#define __PAPER_BOX_DOCUMENT__

#include <vector>
#include <glibmm/ustring.h>

namespace paperbox {

    class Document
    {
    public:
        explicit Document(const Glib::ustring& uri);
        virtual ~Document();

        inline Glib::ustring get_uri() const;

        Glib::ustring get_file_name() const;
        void          set_file_name(const Glib::ustring& file_name);

        Glib::ustring get_modification_time() const;
        void          set_modification_time(const Glib::ustring& modtime);

        guint64       get_mtime() const;
        void          set_mtime(guint64 mtime);

        Glib::ustring get_subject() const;
        void          set_subject(const Glib::ustring& subject);

        Glib::ustring get_author() const;
        void          set_author(const Glib::ustring& author);

        Glib::ustring get_comments() const;
        void          set_comments(const Glib::ustring& comments);

        int           get_page_count() const;
        void          set_page_count(const Glib::ustring& page_count);

        int           get_word_count() const;
        void          set_word_count(const Glib::ustring& word_count);

        Glib::ustring get_date_created() const;
        void          set_date_created(const Glib::ustring& date_created);

        std::vector<Glib::ustring> get_tags();
        void                       set_tags(std::vector<Glib::ustring>& tags);

        bool contains_tag(const Glib::ustring& tag) const;

        void add_tag(const Glib::ustring& tag);
        void remove_tag(const Glib::ustring& tag);

        void print() const;

    protected:
        Glib::ustring uri_;

        Glib::ustring file_name_;
        guint64       mtime_;
        Glib::ustring modtime_;
        Glib::ustring subject_;
        Glib::ustring author_;
        Glib::ustring comments_;
        int page_count_;
        int word_count_;
        Glib::ustring date_created_;

        std::vector<Glib::ustring> tags_;
    };

    /* #@# */

    inline Glib::ustring
    Document::get_uri() const
    {
        return uri_;
    }

} // namespace paperbox

#endif // __PAPER_BOX_DOCUMENT__
