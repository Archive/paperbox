/*
 *  Paperbox - category-factory.hh
 *
 *  Copyright (C) 2007 Marko Anastasov
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#ifndef __PAPERBOX_CATEGORY_FACTORY_HH__
#define __PAPERBOX_CATEGORY_FACTORY_HH__

#include <exception>
#include <list>
#include <string>
#include <tr1/memory>
#include <glibmm/ustring.h>
#include "category.hh"

namespace paperbox {

    class CategoryExists : public std::runtime_error
    {
    public:
        CategoryExists(const std::string& msg) : std::runtime_error(msg) {}
    };
    
    class CategoryNotFound : public std::runtime_error
    {
    public:
        CategoryNotFound(const std::string& msg) : std::runtime_error(msg) {}
    };

    class CategoryFactory
    {
    public:
        // throws CategoryExists
        static std::tr1::shared_ptr<Category> create_category(const Glib::ustring& name);

        // throws CategoryNotFound
        static std::tr1::shared_ptr<Category> get_category(const Glib::ustring& name);

        static std::list<std::tr1::shared_ptr<Category> > load_categories();

        // throws CategoryNotFound
        static void delete_category(const Glib::ustring& name);

        static bool is_name_available(const Glib::ustring& name);

    protected:
        static std::list<std::string> get_categories_from_fs();
    };

} // namespace paperbox

#endif // __PAPERBOX_CATEGORY_FACTORY_HH__
