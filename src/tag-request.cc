// -*- Mode: C++; indent-tabs-mode: nil; c-basic-offset: 4 -*-

/*
 *  PaperBox - tag-request.cc
 *
 *  Copyright (C) 2007 Marko Anastasov
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include <iostream>
#include <glibmm-utils/log-stream-utils.h>
#include <glibmm-utils/ustring.h>
#include "tag-request.hh"

namespace paperbox {

    using std::vector;
    using Glib::ustring;

    TagRequest::TagRequest(TrackerClient* tracker_client,
                           const ustring& uri)
        :
        client_(tracker_client),
        uri_(uri)
    {
    }

    TagRequest::~TagRequest()
    {
    }

    void
    TagRequest::get_results(vector<ustring>& receiver)
    {
        char** results = 0;
        GError* error = 0;

        //LOG_FUNCTION_SCOPE_NORMAL_DD;

        //LOG_DD("Querying tags for " << uri_);

        results = tracker_keywords_get (client_,
                                        SERVICE_DOCUMENTS,
                                        uri_.c_str(),
                                        &error);

        if (error) {
            LOG_ERROR("No tags for " << uri_ << ": " << error->message);
            g_clear_error (&error);
            return;
        }

        if (! results) return;

        char** res_p = 0;

        for (res_p = results; *res_p; res_p++) {
            Glib::ustring found_tag(*res_p);
            Glib::Util::trim(found_tag);

            if (! found_tag.empty())
                receiver.push_back(found_tag);
        }

        //LOG_DD("Got " << static_cast<int>(receiver.size()) << " results");

        g_strfreev (results);
    }

} // namespace paperbox
