/*
 *  Paperbox - config.hh
 *
 *  Copyright (C) 2008 Marko Anastasov
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#ifndef __PAPERBOX_CONFIG__
#define __PAPERBOX_CONFIG__

#include <tr1/memory>
#include <glibmm/miscutils.h>
#include <glibmm/keyfile.h>

namespace paperbox {

    class Config
    {
    public:
        Config() {}
        virtual ~Config() {}

        virtual void get_window_size(int& width, int& height) = 0;
        virtual void set_window_size(const int width, const int height) = 0;
 
        virtual void get_main_paned_position(int& pos) = 0;
        virtual void set_main_paned_position(const int pos) = 0;

        virtual void get_category_paned_position(int& pos) = 0;
        virtual void set_category_paned_position(const int pos) = 0;

        virtual bool has_all_fields() = 0;
        
        virtual void save() = 0;
    };

    std::tr1::shared_ptr<Config> get_default_config();

    void init_config_dirs();

    const std::string KEY_FILE_CONFIG_PATH = Glib::get_home_dir() +
        G_DIR_SEPARATOR_S + ".config" + G_DIR_SEPARATOR + "paperbox" +
        G_DIR_SEPARATOR + "config.ini";

    class KeyFileConfig : public Config
    {
    public:
        explicit KeyFileConfig();
        virtual ~KeyFileConfig();

        virtual void get_window_size(int& width, int& height);
        virtual void set_window_size(const int width, const int height);

        virtual void get_main_paned_position(int& pos);
        virtual void set_main_paned_position(const int pos);

        virtual void get_category_paned_position(int& pos);
        virtual void set_category_paned_position(const int pos);

        virtual bool has_all_fields();
 
        virtual void save();

        std::string get_key_file_path();

    protected:
        void check_file_presence();
        void write_default(bool replace);

        Glib::KeyFile keyfile_;
    };
}

#endif // __PAPERBOX_CONFIG__
