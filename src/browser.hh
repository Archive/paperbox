/*
 *  Paperbox - browser.hh
 *
 *  Copyright (C) 2007 Marko Anastasov
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#ifndef __PAPERBOX_BROWSER__
#define __PAPERBOX_BROWSER__

#include <map>
#include <memory>
#include <tr1/memory>
#include <queue>
#include <vector>
#include <sigc++/sigc++.h>
#include "document.hh"
#include "noncopyable.hh"
#include "tracker-phone.hh"

namespace paperbox {

    typedef std::vector<std::tr1::shared_ptr<Document> > doc_vector;

    enum DocumentSorting {
        DOCUMENT_SORTING_NONE,
        DOCUMENT_SORTING_ALPHABETICAL,
        DOCUMENT_SORTING_BY_DATE_ASC,
        DOCUMENT_SORTING_BY_DATE_DESC
    };

    class Browser : public sigc::trackable, private NonCopyable
    {
    public:
        typedef sigc::signal<void, const std::tr1::shared_ptr<Document>& > SignalDocument;

        typedef sigc::signal<void, const std::vector<std::tr1::shared_ptr<Document> >& > SignalDocuments;

        /*
         * eg void on_tags_changed(
         *           const std::string& uri,
         *           const std::vector<Glib::ustring>& tags_added,
         *           const std::vector<Glib::ustring>& tags_removed);
         */
        typedef sigc::signal<void,
                             const std::string&,
                             const std::vector<Glib::ustring>& ,
                             const std::vector<Glib::ustring>& >
        SignalTagsChanged;

        static Browser* instance();
        virtual ~Browser();

        void add_tags(const Glib::ustring& uri,
                      const std::vector<Glib::ustring>& tags);

        void remove_tags(const Glib::ustring& uri,
                         const std::vector<Glib::ustring>& tags);

        void rename_tag(const Glib::ustring& uri,
                        const Glib::ustring& from_tag,
                        const Glib::ustring& to_tag);

        void get_all_tags(std::list<Glib::ustring>& tags);

        void get_all_documents(doc_vector& docs,
                               DocumentSorting sorting = DOCUMENT_SORTING_NONE);

        void sort_documents(doc_vector& docs,
                            DocumentSorting sorting = DOCUMENT_SORTING_NONE);

        void get_untagged_documents(doc_vector& docs);

        void get_documents_for_tag(const Glib::ustring& tag,
                                   doc_vector& docs);

        void get_documents_for_tag_bundle(
            const std::vector<Glib::ustring>& tags,
            doc_vector& docs);

        sigc::signal<void>& signal_retrieval_started();
        sigc::signal<void>& signal_retrieval_finished();

        SignalDocuments&   signal_new_blank_documents();
        SignalDocument&    signal_document_retrieved();
        SignalTagsChanged& signal_tags_changed();

    protected:
        typedef std::map<std::string, std::tr1::shared_ptr<Document> > doc_map;

        explicit Browser();

        void connect_to_tracker_signals();

        void on_documents(const std::queue<Glib::ustring>& uris);

        bool on_idle_initial_document_retrieval();

        void on_tags_added(const Glib::ustring& uri,
                           const std::vector<Glib::ustring>& tags);

        void on_tags_removed(const Glib::ustring& uri,
                             const std::vector<Glib::ustring>& tags);

        std::tr1::shared_ptr<TrackerPhone> tracker_client_;
        std::queue<std::tr1::shared_ptr<Document> > uri_queue_;
        doc_map docs_;

        sigc::signal<void> signal_retrieval_started_;
        sigc::signal<void> signal_retrieval_finished_;
        SignalDocuments    signal_new_blank_documents_;
        SignalDocument     signal_document_retrieved_;
        SignalTagsChanged  signal_tags_changed_;

    private:
        static std::auto_ptr<Browser> instance_;
    };

} // namespace paperbox

#endif // __PAPERBOX_BROWSER__
