// -*- Mode: C++; indent-tabs-mode: nil; c-basic-offset: 4 -*-

/*
 *  PaperBox - main.cc
 *
 *  Copyright (C) 2007 Marko Anastasov
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include <iostream>
#include <giomm/init.h>
#include <gtkmm/main.h>
#include <glibmm-utils/log-stream-utils.h>
#include "config.hh"
#include "main-window.hh"

int
main(int argc, char** argv)
{
    Glib::thread_init(); // libgnomeui requires it
    Gio::init();
    Gtk::Main kit(argc, argv);

    paperbox::init_config_dirs();

    paperbox::MainWindow* main_window = 0;

    try {
        main_window = paperbox::MainWindow::create();
    }
    catch(const Gnome::Glade::XmlError& ex) {
        LOG_EXCEPTION(ex.what());
        return 1;
    }

    try {
        if (main_window) kit.run(*main_window); 
        delete main_window;
    } catch (const std::exception& ex) {
        LOG_EXCEPTION(ex.what());
    }

    return 0;
}
