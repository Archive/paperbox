/*
 *  Paperbox - category-factory.cc
 *
 *  Copyright (C) 2007 Marko Anastasov
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include <list>
#include <glibmm/fileutils.h>
#include <glibmm/miscutils.h>
#include <glibmm/stringutils.h>
#include <glibmm-utils/log-stream-utils.h>
#include <glibmm-utils/ustring.h>
#include <giomm/file.h>
#include "category-factory.hh"

namespace paperbox {

    using std::list;
    using std::string;
    using std::tr1::shared_ptr;

    struct category_name_compare
        : public std::binary_function<shared_ptr<Category>,
                                      shared_ptr<Category>,
                                      bool>
    {
        bool
        operator()(const shared_ptr<Category>& lhs,
                   const shared_ptr<Category>& rhs) const
            {
                return (lhs->get_name() < rhs->get_name());
            }
    };

    shared_ptr<Category>
    CategoryFactory::create_category(const Glib::ustring& name)
    {
        shared_ptr<Category> cat(new Category(name));
        return cat;
    }

    shared_ptr<Category>
    CategoryFactory::get_category(const Glib::ustring& name)
    {
        if (is_name_available(name))
            throw CategoryNotFound(
                Glib::Util::uprintf("category %s not found", name.c_str()));

        shared_ptr<Category> cat(new Category(name));
        return cat;
    }

    void
    CategoryFactory::delete_category(const Glib::ustring& name)
    {
        if (is_name_available(name))
            throw CategoryNotFound(
                Glib::Util::uprintf("category %s not found", name.c_str()));

        Glib::RefPtr<Gio::File> file =
            Gio::File::create_for_path(Category::get_default_path() + name);

        if (! file->remove()) {
            LOG_ERROR("Unable to remove category file " << file->get_path());
        }
    }

    list<string>
    CategoryFactory::get_categories_from_fs()
    {
        list<string> categories;
        string path = Category::get_default_path();

        try {
            Glib::Dir dir(path);
            list<string> items(dir.begin(), dir.end());

            list<string>::iterator it(items.begin());
            list<string>::iterator end(items.end());
		
            for ( ; it != end; ++it) {
                string file = Glib::build_filename(path, *it);
			
                if (Glib::file_test(file, Glib::FILE_TEST_IS_REGULAR))
                    categories.push_back(*it);
            }
        }
        catch (Glib::FileError& ex) {
            LOG_EXCEPTION("Cannot open category directory "
                          << path << ": " << Glib::strerror(ex.code()));
        }

        return categories;
    }

    bool
    CategoryFactory::is_name_available(const Glib::ustring& name)
    {
        list<string> categories(get_categories_from_fs());
        list<string>::iterator end(categories.end());

        if (find(categories.begin(), end, name) == end)
            return true;

        return false;
    }

    list<shared_ptr<Category> >
    CategoryFactory::load_categories()
    {
        list<shared_ptr<Category> > categories;
        list<string> category_names(get_categories_from_fs());

        list<string>::iterator it(category_names.begin());
        list<string>::iterator end(category_names.end());

        for ( ; it != end; ++it) {
            shared_ptr<Category> cat = create_category(*it);
            categories.push_back(cat);
        }

        categories.sort(category_name_compare());

        return categories;
    }

} // namespace paperbox

