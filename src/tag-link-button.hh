// -*- Mode: C++; indent-tabs-mode: nil; c-basic-offset: 4 -*-

/*
 *  PaperBox - tag-link-button.hh
 *
 *  Copyright (C) 2007 Marko Anastasov
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#ifndef __PAPER_BOX_TAG_LINK_BUTTON_HH__
#define __PAPER_BOX_TAG_LINK_BUTTON_HH__

#include <gtkmm/actiongroup.h>
#include <gtkmm/linkbutton.h>
#include <gtkmm/menu.h>
#include <gtkmm/uimanager.h>

namespace paperbox {

    class TagLinkButton : public Gtk::LinkButton
    {
    public:
        typedef sigc::signal<void, const Glib::ustring&> SignalTagRemoveRequest;

        explicit TagLinkButton(const Glib::ustring& tag);
        virtual ~TagLinkButton();

        SignalTagRemoveRequest& signal_tag_remove_request();

    protected:
        virtual bool on_button_press_event(GdkEventButton* event);

        virtual void on_popup_remove();

        virtual void fill_menu();

        Glib::ustring tag_;

        Glib::RefPtr<Gtk::UIManager>   ui_manager_;
        Glib::RefPtr<Gtk::ActionGroup> action_group_;

        Gtk::Menu* popup_menu_;

        SignalTagRemoveRequest signal_tag_remove_request_;
    };

} // namespace paperbox

#endif // __PAPER_BOX_TAG_LINK_BUTTON_HH__
