/*
 *  Paperbox - main-window.cc
 *
 *  Copyright (C) 2007-2008 Marko Anastasov
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include <config.h>
#include <iostream>
#include <list>
#include <map>
#include <vector>
#include <glib/gi18n.h>
#include <glibmm/markup.h>
#include <gtkmm/aboutdialog.h>
#include <glibmm-utils/ustring.h>
#include <gtkmm-utils/tile.h>
#include "browser.hh"
#include "category-editor.hh"
#include "category-factory.hh"
#include "config.hh"
#include "file-utils.hh"
#include "dialog-properties.hh"
#include "document-tag-cloud-model.hh"
#include "document-tile.hh"
#include "i18n-utils.hh"
#include "main-window.hh"
#include "paths.hh"

namespace paperbox {

    using std::list;
    using std::map;
    using std::vector;
    using std::tr1::shared_ptr;

    ///

    class CategoryModel
    {
    public:
        explicit CategoryModel() {}
        ~CategoryModel() {}

        std::list<shared_ptr<Category> > load_data();

        shared_ptr<Category> get_category(const Glib::ustring& name);

    protected:
        std::map<Glib::ustring, shared_ptr<Category> > data_;
    };

    ///

    list<shared_ptr<Category> >
    CategoryModel::load_data()
    {
        list<shared_ptr<Category> > categories(
            CategoryFactory::load_categories());

        list<shared_ptr<Category> >::iterator it(categories.begin());
        list<shared_ptr<Category> >::iterator end(categories.end());

        for ( ; it != end; ++it)
            data_[(*it)->get_name()] = *it;

        return categories;
    }

    shared_ptr<Category>
    CategoryModel::get_category(const Glib::ustring& name)
    {
        shared_ptr<Category> ptr;
        map<Glib::ustring, shared_ptr<Category> >::iterator it = 
            data_.find(name);

        if (it != data_.end())
            ptr = it->second;

        return ptr;
    }

    ///
    
    enum BROWSING_MODE {
        BROWSING_ALPHABETICAL,
        BROWSING_BY_DATE_ASC,
        BROWSING_BY_DATE_DESC
    };

    const int DOCS_ALL = 1;
    const int DOCS_UNTAGGED = 2;

    MainWindow::MainWindow(GtkWindow* cobject,
                           const Glib::RefPtr<Gnome::Glade::Xml>& glade)
        :
        Gtk::Window(cobject),
        glade_(glade),
        in_retrieval_(false),
        hpane_(0),
        left_top_vbox_(0),
        tile_view_(0),
        right_top_vbox_(0),
        right_vpane_(0),
        category_vbox_(0),
        tag_cloud_vbox_(0),
        tag_box_(false, 4)
    {
        shared_ptr<Config> cfg = get_default_config();
        init_gui(cfg);

        set_default_icon_name("paperbox");

        set_position(Gtk::WIN_POS_CENTER_ALWAYS);

        int width, height;
        cfg->get_window_size(width, height);
        set_default_size(width, height);

        int pane_pos;
        cfg->get_main_paned_position(pane_pos);
        hpane_->set_position(pane_pos);

        browser_ = Browser::instance();
        tiles_.reset(new TileSet());
        category_model_.reset(new CategoryModel());
        reload_category_view();

        connect_signals();
    }

    MainWindow::~MainWindow()
    {
        // save layout in the config file
        shared_ptr<Config> cfg = get_default_config();

        int width, height;
        get_size(width, height);
        cfg->set_window_size(width, height);

        int pos = hpane_->get_position();
        cfg->set_main_paned_position(pos);

        pos = right_vpane_->get_position();
        cfg->set_category_paned_position(pos);

        cfg->save();
    }

    MainWindow*
    MainWindow::create()
    {
        Glib::RefPtr<Gnome::Glade::Xml> glade_xml =
            Gnome::Glade::Xml::create(glade_window_main);

        MainWindow* p = 0;
        glade_xml->get_widget_derived("MainWindow", p);
        return p;
    }

    void
    MainWindow::init_gui(const shared_ptr<Config>& cfg)
    {
        get_widgets_from_ui_file();
        init_toolbar();
        init_sorting_area();
        setup_tiles();

        label_tags_.set_markup(boldify(_("Tags")));
        tag_cloud_vbox_->pack_start(label_tags_, false, false);

        model_.reset(new DocumentTagCloudModel(10, 18));
        tag_cloud_.set_model(model_);
        tag_cloud_vbox_->pack_start(tag_box_);

        tag_box_.pack_start(tag_cloud_, true, true);

        setup_categories();

        int paned_pos;
        cfg->get_category_paned_position(paned_pos);
        right_vpane_->set_position(paned_pos);

        set_title(Glib::Util::uprintf("%s %s", PACKAGE_NAME, PACKAGE_VERSION));

        show_all_children();
    }

    void
    MainWindow::get_widgets_from_ui_file()
    {
        // make sure the glade object has been initialized properly in the ctor
        g_assert(glade_);

        glade_->get_widget("menu_vbox", menu_vbox_);
        g_assert(menu_vbox_);

        glade_->get_widget("hpane", hpane_);
        g_assert(hpane_);

        glade_->get_widget("left_top_vbox", left_top_vbox_);
        g_assert(left_top_vbox_);

        glade_->get_widget("right_top_vbox", right_top_vbox_);
        g_assert(right_top_vbox_);

        glade_->get_widget("right_vpane", right_vpane_);
        g_assert(right_vpane_);

        glade_->get_widget("category_vbox", category_vbox_);
        g_assert(category_vbox_);

        glade_->get_widget("tag_cloud_vbox", tag_cloud_vbox_);
        g_assert(tag_cloud_vbox_);
    }

    void
    MainWindow::init_toolbar()
    {
        action_group_ = Gtk::ActionGroup::create();

        // actions
        action_group_->add(Gtk::Action::create("DocumentMenu", _("_Document")));

        action_group_->add(
                Gtk::Action::create("DocumentOpen", Gtk::Stock::OPEN),
                sigc::mem_fun(*this, &MainWindow::on_menu_document_open));

        action_group_->add(
                Gtk::Action::create("DocumentTag", Gtk::Stock::ADD, _("Add _Tag")),
                Gtk::AccelKey("<control>T"),
                sigc::mem_fun(*this, &MainWindow::on_menu_document_tag));

        action_group_->add(
                Gtk::Action::create("DocumentProperties",
                    Gtk::Stock::PROPERTIES),
                Gtk::AccelKey("<control>E"),
                sigc::mem_fun(*this, &MainWindow::on_menu_document_properties));

        action_group_->add(
                Gtk::Action::create("DocumentQuit", Gtk::Stock::QUIT),
                sigc::mem_fun(*this, &MainWindow::on_menu_document_quit));

        action_group_->add(Gtk::Action::create("ViewMenu", _("_View")));

        action_group_->add(
                Gtk::Action::create("ViewFirstPage",
                    Gtk::Stock::GOTO_FIRST),
                Gtk::AccelKey("<Alt>Up"),
                sigc::mem_fun(*this, &MainWindow::on_menu_view_first_page));

        action_group_->add(
                Gtk::Action::create("ViewPreviousPage",
                    Gtk::Stock::GO_BACK),
                Gtk::AccelKey("<Alt>Left"),
                sigc::mem_fun(*this, &MainWindow::on_menu_view_previous_page));
        
        action_group_->add(
                Gtk::Action::create("ViewNextPage",
                    Gtk::Stock::GO_FORWARD),
                Gtk::AccelKey("<Alt>Right"),
                sigc::mem_fun(*this, &MainWindow::on_menu_view_next_page));

        action_group_->add(
                Gtk::Action::create("ViewLastPage",
                    Gtk::Stock::GOTO_LAST),
                Gtk::AccelKey("<Alt>Down"),
                sigc::mem_fun(*this, &MainWindow::on_menu_view_last_page));

        action_group_->add(Gtk::Action::create("HelpMenu", _("_Help")));

        action_group_->add(
                Gtk::Action::create("HelpAbout", Gtk::Stock::ABOUT),
                sigc::mem_fun(*this, &MainWindow::on_menu_about));

        ui_manager_ = Gtk::UIManager::create();
        ui_manager_->insert_action_group(action_group_);

        /*this->*/add_accel_group(ui_manager_->get_accel_group());

        Glib::ustring ui_info =
            "<ui>"
            "   <menubar name='MenuBar'>"
            "       <menu action='DocumentMenu'>"
            "           <menuitem action='DocumentOpen'/>"
            "           <menuitem action='DocumentTag'/>"
            "           <separator/>"
            "           <menuitem action='DocumentProperties'/>"
            "           <separator/>"
            "           <menuitem action='DocumentQuit'/>"
            "       </menu>"
            "       <menu action='ViewMenu'>"
            "           <menuitem action='ViewFirstPage'/>"
            "           <menuitem action='ViewPreviousPage'/>"
            "           <menuitem action='ViewNextPage'/>"
            "           <menuitem action='ViewLastPage'/>"
            "       </menu>"
            "       <menu action='HelpMenu'>"
            "           <menuitem action='HelpAbout'/>"
            "       </menu>"
            "   </menubar>"
            "   <toolbar name='ToolBar'>"
            "       <toolitem action='DocumentTag'/>"
            "       <toolitem action='HelpAbout'/>"
            "   </toolbar>"
            "</ui>";
        
        try {
            ui_manager_->add_ui_from_string(ui_info);
        } catch (const Glib::Error& ex) {
            Glib::ustring msg =
                "building menu and toolbar failed: " + ex.what();
            LOG_ERROR(msg);
        }

        Gtk::Widget* menubar = ui_manager_->get_widget("/MenuBar");
        Gtk::Widget* toolbar = ui_manager_->get_widget("/ToolBar");
        if (menubar && toolbar) {
            menu_vbox_->pack_start(*menubar, Gtk::PACK_SHRINK);
            //menu_vbox_->pack_start(*toolbar, Gtk::PACK_SHRINK);
        } else {
            LOG_ERROR("could not get menu and toolbar widgets from UIManager");
        }
    }

    void
    MainWindow::init_sorting_area()
    {
        sorting_label_.set_text(_("Browse"));

        sorting_combo_.append_text(_("alphabetically"));
        sorting_combo_.append_text(_("by date, oldest first"));
        sorting_combo_.append_text(_("by date, newest first"));

        sorting_combo_.signal_changed().connect(
                sigc::mem_fun(*this, &MainWindow::on_sorting_changed));

        sorting_hbox_.set_spacing(5);
        sorting_hbox_.pack_start(sorting_label_, false, false);
        sorting_hbox_.pack_start(sorting_combo_, false, false);

        left_top_vbox_->pack_start(sorting_hbox_, false, false);
    }

    void
    MainWindow::setup_tiles()
    {
        tile_view_ = Gtk::manage(new DocumentTileView());
        tile_view_->set_navigator_title(_("Showing documents"));
        tile_view_->set_tiles_per_page(7);

        left_top_vbox_->pack_start(*tile_view_);
    }

    void
    MainWindow::setup_categories()
    {
        category_view_.reset(new CategoryView(category_vbox_));

        category_view_->selection->set_select_function(
            sigc::mem_fun(*this, &MainWindow::on_category_selected));
    }

    void
    MainWindow::on_menu_document_open()
    {
        DocumentTile* tile =
            dynamic_cast<DocumentTile*>(tile_view_->get_selection());
        if (! tile) return;

        on_document_tile_selected(*tile);
    }

    void
    MainWindow::on_menu_document_tag()
    {
        DocumentTile* tile =
            dynamic_cast<DocumentTile*>(tile_view_->get_selection());
        if (! tile) return;

        tile->show_add_tag_dialog();
    }

    void
    MainWindow::on_menu_document_properties()
    {
        DocumentTile* tile =
            dynamic_cast<DocumentTile*>(tile_view_->get_selection());
        if (! tile) return;

        shared_ptr<DialogProperties> dialog(
                DialogProperties::create());
        shared_ptr<Document> doc = tile->get_document();
        dialog->run(doc);
    }

    void
    MainWindow::on_menu_document_quit()
    {
        hide();
    }

    void
    MainWindow::on_menu_view_first_page()
    {
        tile_view_->get_page_navigator().get_button_first().clicked();
    }

    void
    MainWindow::on_menu_view_previous_page()
    {
        tile_view_->get_page_navigator().get_button_previous().clicked();
    }

    void
    MainWindow::on_menu_view_next_page()
    {
        tile_view_->get_page_navigator().get_button_next().clicked();
    }

    void
    MainWindow::on_menu_view_last_page()
    {
        tile_view_->get_page_navigator().get_button_last().clicked();
    }

    void
    MainWindow::on_menu_about()
    {
        Gtk::AboutDialog dialog;

        dialog.set_program_name(PACKAGE_NAME);
        dialog.set_version(PACKAGE_VERSION);
        dialog.set_comments(_("A document browser for GNOME"));

        list<Glib::ustring> authors;
        authors.push_back("Marko Anastasov <marko.anastasov@gmail.com>");
        dialog.set_authors(authors);
        dialog.set_copyright("Copyright \xc2\xa9 2007-2009 Marko Anastasov");

        dialog.set_website("http://live.gnome.org/PaperBox");

        Glib::ustring license =
            "This program is free software; you can redistribute it and/or modify\n"
            "it under the terms of the GNU General Public License as published by\n"
            "the Free Software Foundation; either version 2 of the License, or\n"
            "(at your option) any later version.\n\n"

            "This program is distributed in the hope that it will be useful,\n"
            "but WITHOUT ANY WARRANTY; without even the implied warranty of\n"
            "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n"
            "GNU General Public License for more details.\n\n"

            "You should have received a copy of the GNU General Public License\n"
            "along with this program; if not, write to the \n"
            "Free Software Foundation, Inc., 59 Temple Place, Suite 330, \n"
            "Boston, MA  02111-1307  USA\n";
        dialog.set_license(license);

        // Translators: change this to your name, separate multiple names with \n
        dialog.set_translator_credits(_("translator-credits"));

        Glib::RefPtr<Gtk::IconTheme> theme = Gtk::IconTheme::get_default();
        if (theme->has_icon("paperbox")) {
            Glib::RefPtr<Gdk::Pixbuf> icon =
                theme->load_icon("paperbox", 128, Gtk::ICON_LOOKUP_USE_BUILTIN);
            dialog.set_logo(icon);
        }

        vector<Glib::ustring> artists;
        artists.push_back("Jakub Szypulka <cube@szypulka.com>");
        dialog.set_artists(artists);

        dialog.run();
    }

    bool
    MainWindow::on_category_selected(const Glib::RefPtr<Gtk::TreeModel>& /*m*/,
                                     const Gtk::TreeModel::Path& path,
                                     bool path_selected)
    {
        Gtk::TreeModel::Row row = *(category_view_->treemodel->get_iter(path));
        int id = row[category_view_->columns.col_id];
        Glib::ustring cat_name = row[category_view_->columns.col_name];

        if (! ((cat_name != selected_cat_name_) && (! path_selected)))
            return true; // selection hasn't changed

        selected_cat_name_ = cat_name;

        vector<shared_ptr<Document> > docs;

        // decide which set of documents to display
        if (id == DOCS_ALL)
            browser_->get_all_documents(docs);
        else if (id == DOCS_UNTAGGED)
            browser_->get_untagged_documents(docs);
        else {
            shared_ptr<Category> cat =
                category_model_->get_category(selected_cat_name_);

            if (! cat.get())
                g_warning("Invalid selection for category %s",
                          selected_cat_name_.c_str());
            else {
                vector<Glib::ustring> tags = cat->get_tags();
                browser_->get_documents_for_tag_bundle(tags, docs);
            }
        }

        sort_documents(docs);
        render_documents(docs);

        return true;
    }

    void
    MainWindow::reload_category_view()
    {
        category_view_->treemodel->clear();

        Gtk::TreeModel::Row row;

        row = *(category_view_->treemodel->append());
        row[category_view_->columns.col_id] = DOCS_ALL;
        //Translators: 'All' means 'All documents'
        row[category_view_->columns.col_name] = _("All");

        row = *(category_view_->treemodel->append());
        row[category_view_->columns.col_id] = DOCS_UNTAGGED;
        //'Untagged documents'
        row[category_view_->columns.col_name] = _("Untagged");


        list<shared_ptr<Category> > categories(category_model_->load_data());
        
        list<shared_ptr<Category> >::iterator it(categories.begin());
        list<shared_ptr<Category> >::iterator end(categories.end());

        for ( ; it != end; ++it) {
            row = *(category_view_->treemodel->append());
            row[category_view_->columns.col_name] = (*it)->get_name();
        }

        category_view_->select_first();
    }

    void
    MainWindow::connect_signals()
    {
        browser_->signal_retrieval_started().connect(
            sigc::mem_fun(*this, &MainWindow::on_retrieval_started));

        browser_->signal_retrieval_finished().connect(
            sigc::mem_fun(*this, &MainWindow::on_retrieval_finished));

        browser_->signal_new_blank_documents().connect(
            sigc::mem_fun(*this, &MainWindow::on_new_blank_documents));

        browser_->signal_document_retrieved().connect(
            sigc::mem_fun(*this, &MainWindow::on_document_retrieved));

        tile_view_->signal_tile_activated().connect(
            sigc::mem_fun(*this, &MainWindow::on_document_tile_selected));

        category_view_->button_edit.signal_clicked().connect(
            sigc::mem_fun(*this, &MainWindow::on_edit_category));

        tile_view_->signal_tag_clicked().connect(
            sigc::mem_fun(*this, &MainWindow::on_tag_clicked));

        tag_cloud_.signal_tag_clicked().connect(
            sigc::mem_fun(*this, &MainWindow::on_tag_clicked));
    }

    void
    MainWindow::on_retrieval_started()
    {
        in_retrieval_ = true;
        hide_widgets_in_retrieval();
    }

    void
    MainWindow::on_retrieval_finished()
    {
        in_retrieval_ = false;
        show_widgets_after_retrieval();
    }

    void
    MainWindow::on_new_blank_documents(const vector<shared_ptr<Document> >& docs)
    {
        vector<shared_ptr<Document> >::const_iterator it(docs.begin());
        vector<shared_ptr<Document> >::const_iterator end(docs.end());
        for ( ; it != end; ++it) {
            shared_ptr<DocumentTile> tile(new DocumentTile(thumbnailer_, *it));

            bool ok = tiles_->add(tile);
            if (! ok) return; // maybe not?

            tile_view_->add_tile(*tile);
        }
    }

    // Invoked from Browser during idle time.
    // All this should be re-thought when we get xesam dbus api in tracker.
    void
    MainWindow::on_document_retrieved(const shared_ptr<Document>& doc)
    {
        shared_ptr<DocumentTile> tile(new DocumentTile(thumbnailer_, doc));

        std::vector<Glib::ustring> tags = doc->get_tags();
        std::vector<Glib::ustring>::iterator it(tags.begin());
        std::vector<Glib::ustring>::iterator end(tags.end());

        for ( ; it != end; ++it)
            model_->add_tag(*it);
    }

    void
    MainWindow::on_document_tile_selected(/*Document*/Gtk::Util::Tile& t)
    {
        DocumentTile* dt = dynamic_cast<DocumentTile*>(&t);
        open_file_with_xdg(dt->get_document_uri());
    }

    void
    MainWindow::render_new_tile_set(const vector<shared_ptr<Document> >& docs)
    {
        tile_view_->clear();

        vector<shared_ptr<Document> >::const_iterator it(docs.begin());
        vector<shared_ptr<Document> >::const_iterator end(docs.end());

        for ( ; it != end; ++it) {
            std::string uri = (*it)->get_uri().raw();
            shared_ptr<DocumentTile> tile = tiles_->get_tile(uri);
            if (tile) tile_view_->add_tile(*tile);
        }
    }

    void
    MainWindow::on_tag_clicked(const Glib::ustring& tag)
    {
        vector<shared_ptr<Document> > docs;
        browser_->get_documents_for_tag(tag, docs);
        sort_documents(docs);

        tile_view_->reset_selection();
        render_new_tile_set(docs);

        category_view_->selection->unselect_all();
        selected_cat_name_ = "";

        // "manually" put a marker on the tag in the cloud
        tag_cloud_.select_tag(tag);
    }

    void
    MainWindow::on_edit_category()
    {
        shared_ptr<CategoryEditor> dialog(CategoryEditor::create());
        dialog->set_default_response(Gtk::RESPONSE_OK);

        dialog->run();

        reload_category_view();
    }

    void
    MainWindow::render_documents(const vector<shared_ptr<Document> > docs)
    {
        render_new_tile_set(docs);
        tag_cloud_.reset_selection();
    }

    void
    MainWindow::on_sorting_changed()
    {
        doc_vector docs;
        tile_view_->get_displayed_documents(docs);
        sort_documents(docs);

        render_documents(docs);
    }

    void
    MainWindow::hide_widgets_in_retrieval()
    {
        sorting_hbox_.hide_all();
    }

    void
    MainWindow::show_widgets_after_retrieval()
    {
        sorting_hbox_.show_all();
    }

    // When we retrieve a list of documents from Browser after a certain
    // category or tag has been selected, we need to sort them based on
    // user's current sorting/browsing preference, if any.
    void
    MainWindow::sort_documents(doc_vector& docs)
    {
        int sorting = sorting_combo_.get_active_row_number();

        if (sorting == BROWSING_ALPHABETICAL) {
            browser_->sort_documents(docs,
                                     DOCUMENT_SORTING_ALPHABETICAL);
        } else if (sorting == BROWSING_BY_DATE_ASC) {
            browser_->sort_documents(docs,
                                     DOCUMENT_SORTING_BY_DATE_ASC);
        } else if (sorting == BROWSING_BY_DATE_DESC) {
            browser_->sort_documents(docs,
                                     DOCUMENT_SORTING_BY_DATE_DESC);
        }
    }

} // namespace paperbox
