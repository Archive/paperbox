// -*- Mode: C++; indent-tabs-mode: nil; c-basic-offset: 4 -*-

/*
 * PaperBox - tag-item.cc
 *
 * Copyright (C) 2007 Marko Anastasov
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "tag-item.hh"

namespace paperbox {

    TagItem::TagItem(TagCloud* parent, GooCanvasItem* text_item,
                     Glib::ustring tag, FillColor fill_color)
        : parent_(parent),
          text_item_(text_item),
          rect_marker_(0),
          tag_(tag),
          color_(fill_color)
    {
        goo_canvas_item_get_bounds (text_item, &bounds_);
    }

    void
    TagItem::marker_on()
    {
        if (! rect_marker_) {
            GooCanvasItem* new_marker = create_rectangle_marker();
            set_marker(new_marker);
        }

        g_object_set (rect_marker_,
                      "visibility", GOO_CANVAS_ITEM_VISIBLE,
                      NULL);
    }

    void
    TagItem::marker_off()
    {
        if (! rect_marker_) return;
        g_object_set (rect_marker_,
                      "visibility", GOO_CANVAS_ITEM_INVISIBLE,
                      NULL);
    }

    GooCanvasItem*
    TagItem::create_rectangle_marker()
    {
        GooCanvasItem* new_marker;

        new_marker = goo_canvas_rect_new (get_parent_item(),
                                          get_left() - MARKER_SPACING_HALF,
                                          get_top() - MARKER_SPACING_HALF,
                                          get_width() + MARKER_SPACING,
                                          get_height() + MARKER_SPACING,
                                          NULL);

        return new_marker;
    }

} // namespace paperbox
