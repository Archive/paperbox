// -*- Mode: C++; indent-tabs-mode: nil; c-basic-offset: 4 -*-

/*
 *  PaperBox - category-view.cc
 *
 *  Copyright (C) 2008 Marko Anastasov
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include <glib/gi18n.h>
#include "category-view.hh"
#include <gtkmm/stock.h>
#include "i18n-utils.hh"

namespace paperbox {

    CategoryView::CategoryView(Gtk::Box* _parent, bool edit_enabled)
        :
        parent(_parent),
        button_edit(Gtk::Stock::EDIT)
    {
        // pack the widgets
        label_title.set_markup(boldify(_("Categories")));
        title_box.pack_start(label_title);
        if (edit_enabled)
            title_box.pack_start(button_edit, false, false);
        parent->pack_start(title_box, false, false);

        scroll.set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC);
        scroll.add(treeview);
        parent->pack_start(scroll, true, true);

        // create the tree model
        treemodel = Gtk::ListStore::create(columns);
        treeview.set_model(treemodel);

        // set up the treeview selection object
        selection = treeview.get_selection();
        selection->set_mode(Gtk::SELECTION_SINGLE);

        // add a category column
        treeview.append_column(_("Name"), columns.col_name);
    }

    /// selects the first list item, if any
    void
    CategoryView::select_first()
    {
        if (! treemodel->children().empty())
            selection->select(treemodel->children().begin());
    }

} // namespace paperbox
