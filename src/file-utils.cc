// -*- Mode: C++; indent-tabs-mode: nil; c-basic-offset: 4 -*-

/*
 *  PaperBox - file-utils.cc
 *
 *  Copyright (C) 2007 Marko Anastasov
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include <glib.h>
#include <gio/gio.h>
#include <glibmm/convert.h>
#include <glibmm-utils/log-stream-utils.h>
#include "file-utils.hh"

namespace paperbox {

    bool
    open_file_with_xdg (const Glib::ustring& file)
    {
	bool   result;
	gchar* quoted_filename = g_shell_quote (file.c_str());
	gchar* command = g_strconcat ("xdg-open ", quoted_filename, NULL);
        
	g_free (quoted_filename);
	result = g_spawn_command_line_async (command, NULL);
	g_free (command);

        return result;
    }

    void
    get_file_modification_time(const Glib::ustring& uri,
                               guint64& modtime,
                               Glib::ustring& modtime_string)
    {
        GFile* file;
        GFileQueryInfoFlags flags = G_FILE_QUERY_INFO_NONE;
        GFileInfo* info;
        GError* error = 0;

        file = g_file_new_for_path (uri.c_str());

        info = g_file_query_info (file,
                                  G_FILE_ATTRIBUTE_TIME_MODIFIED,
                                  flags,
                                  NULL,
                                  &error);

        if (info == NULL) {
            LOG_ERROR("couldn't gett time info: %s\n" << error->message);
            g_error_free (error);
        }

        struct timeval tv;
        struct tm* ptm;
        char time_string[256];

        modtime = g_file_info_get_attribute_uint64 (info, "time::modified");
        tv.tv_sec = modtime;

        ptm = localtime(&tv.tv_sec);
        strftime(time_string, sizeof(time_string), "%c", ptm);

        modtime_string = Glib::locale_to_utf8(time_string);
    }

} // namespace paperbox
