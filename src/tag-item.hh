// -*- Mode: C++; indent-tabs-mode: nil; c-basic-offset: 4 -*-

/*
 * PaperBox - tag-item.hh
 *
 * Copyright (C) 2007 Marko Anastasov
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef __TAG_ITEM_HH__
#define __TAG_ITEM_HH__

#include <goocanvas.h>
#include <glibmm/ustring.h>

namespace paperbox {

    static const gdouble MARKER_SPACING = 4.0;
    static const gdouble MARKER_SPACING_HALF = 2.0;

    enum FillColor
    {
        FILL_COLOR_BLUE       = 0x0000ffff,
        FILL_COLOR_LIGHT_BLUE = 0x6060ffff,
        FILL_COLOR_RED        = 0xff0000ff
    };

    class TagCloud;

    /// A tag, ie canvas item, in a TagCloud.
    class TagItem
    {
    public:
        explicit TagItem(TagCloud* parent,
                         GooCanvasItem* text_item,
                         Glib::ustring tag,
                         FillColor fill_color);

        ~TagItem() {} // the parent (TagCloud) takes care of the GooCanvasItems

        TagCloud*      get_parent()       { return parent_; }
        GooCanvasItem* get_parent_item()
            { return goo_canvas_item_get_parent (text_item_); }
        GooCanvasItem* get_canvas_item()  { return text_item_; }
        GooCanvasItem* get_marker()       { return rect_marker_; }

        Glib::ustring  get_tag()         const { return tag_; }
        guint          get_fill_colour() const { return color_; }

        gdouble        get_left()   const { return bounds_.x1; }
        gdouble        get_top()    const { return bounds_.y1; }
        gdouble        get_right()  const { return bounds_.x2; }
        gdouble        get_bottom() const { return bounds_.y2; }
        gdouble        get_width()  const { return bounds_.x2 - bounds_.x1; }
        gdouble        get_height() const { return bounds_.y2 - bounds_.y1; }

        void set_marker(GooCanvasItem* new_marker)
            { rect_marker_ = new_marker; }

        void marker_on();
        void marker_off();

        GooCanvasItem* create_rectangle_marker();

    protected:
        TagCloud*       parent_;
        GooCanvasItem*  text_item_;
        GooCanvasBounds bounds_;
        GooCanvasItem*  rect_marker_;
        Glib::ustring   tag_;
        FillColor       color_;
    };

} // namespace paperbox

#endif // __TAG_ITEM_HH__
