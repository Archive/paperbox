/*
 *  Paperbox - category-editor.hh
 *
 *  Copyright (C) 2008 Marko Anastasov
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#ifndef __PAPERBOX_CATEGORY_EDITOR_HH__
#define __PAPERBOX_CATEGORY_EDITOR_HH__

#include <tr1/memory>
#include <gtkmm/button.h>
#include <gtkmm/dialog.h>
#include <gtkmm/label.h>
#include <gtkmm/scrolledwindow.h>
#include <gtkmm/textview.h>
#include <libglademm/xml.h>
#include "category-view.hh"
#include "tag-cloud.hh"
#include "tag-cloud-model.hh"

namespace paperbox {

    class CategoryEditorData;
    class CategoryEditorModel;
    class Document;

    class CategoryEditor : public Gtk::Dialog
    {
    public:
        CategoryEditor(GtkDialog* cobject,
                       const Glib::RefPtr<Gnome::Glade::Xml>& glade);
        virtual ~CategoryEditor();

        static CategoryEditor* create();

        int run();

    protected:
        void get_widgets();
        void init_gui();
        void connect_signals();

        void load_tags();
        void load_categories();
        void add_new_row(std::tr1::shared_ptr<CategoryEditorData>& data);

        void check_buffer_status();

        void on_button_new_clicked();
        void on_button_delete_clicked();
        void on_button_save_clicked();
        bool on_category_selected(const Glib::RefPtr<Gtk::TreeModel>& model,
                                  const Gtk::TreeModel::Path& path,
                                  bool path_selected);
        bool on_key_release_event(GdkEventKey* key);
        void on_document_retrieved(const std::tr1::shared_ptr<Document>& doc);
        void on_tag_clicked(const Glib::ustring& tag);

        virtual bool on_expose_event(GdkEventExpose* event);

        Glib::RefPtr<Gnome::Glade::Xml> glade_;

        // containers defined in glade
        Gtk::VBox* vbox_left_;
        Gtk::VBox* vbox_right_;
        Gtk::HBox* hbox_contents_;

        std::tr1::shared_ptr<CategoryView> category_view_;
        Glib::ustring selected_name_;
        Gtk::Button button_new_;
        Gtk::Button button_delete_;

        Gtk::Label  label_category_tags_;
        Gtk::Button button_save_;
        Gtk::ScrolledWindow scroll_window_;
        Gtk::TextView text_view_;

        // tag cloud
        Gtk::VBox   tag_box_;
        TagCloud    tag_cloud_;
        std::tr1::shared_ptr<TagCloudModel> tag_cloud_model_;
        bool initial_tag_load_complete_;

        // the data model
        std::tr1::shared_ptr<CategoryEditorModel> model_;
    };

} // namespace paperbox

#endif // __PAPERBOX_CATEGORY_EDITOR_HH__
