/*
 * Paperbox - tag-cloud.hh
 *
 * Copyright (C) 2007 Marko Anastasov
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef __TAG_CLOUD_HH__
#define __TAG_CLOUD_HH__

#include <list>
#include <map>
#include <tr1/memory>
#include <glib/grand.h>
#include <gtkmm.h>
#include "tag-cloud-model.hh"

namespace paperbox {

class TagItem;

/// Tag cloud widget based on GooCanvas.
class TagCloud : public Gtk::ScrolledWindow
{
public:
    typedef sigc::signal<void, const Glib::ustring&> SignalTagClicked;

    explicit TagCloud();
    virtual ~TagCloud();

    void set_model(std::tr1::shared_ptr<TagCloudModel> model);

    void set_use_random_line_borders(bool randomize = true)
        { randomize_borders_ = randomize; }
    bool get_random_line_borders() const { return randomize_borders_; }

    void set_underline_tags(bool underline = true)
        { underline_tags_ = underline; }
    bool get_underline_tags() const { return underline_tags_; }

    void set_lighten_rare_tags(bool lighten = true)
        { lighten_rare_tags_ = lighten; }
    bool get_lighten_rare_tags() const { return lighten_rare_tags_; }

    void set_mark_selected_tags(bool mark = true)
        { mark_selected_ = mark; }
    bool get_mark_selected_tags() const { return mark_selected_; }

    SignalTagClicked& signal_tag_clicked() { return signal_tag_clicked_; }

    void update_marker(TagItem* new_selection);

    void reset_selection(bool keep_old_selection = false);

    void select_tag(const Glib::ustring& tag);

protected:
    virtual void on_size_allocate(Gdk::Rectangle& allocation);

    virtual void remove_canvas_items();

    virtual void redraw_tags();

    virtual void on_model_changed();

    virtual void set_tags(const std::map<Glib::ustring, int>& tags);

    SignalTagClicked signal_tag_clicked_;

    Gtk::Widget* canvas_;
    std::list<std::tr1::shared_ptr<TagItem> > text_items_;
    std::tr1::shared_ptr<TagCloudModel> model_;
    TagItem* selection_;
    Glib::ustring selection_string_;

    GRand* grand_;
    bool   randomize_borders_;

    bool   underline_tags_;
    bool   lighten_rare_tags_;
    bool   mark_selected_;

    int alloc_width_;
    int alloc_height_;

private:
    TagCloud(const TagCloud& );
    TagCloud& operator=(const TagCloud& );
};

} // namespace paperbox

#endif // __TAG_CLOUD_HH__
