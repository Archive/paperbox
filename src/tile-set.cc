/*
 *  Paperbox - tile-set.cc
 *
 *  Copyright (C) 2007 Marko Anastasov
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "tile-set.hh"

namespace paperbox {

    using std::string;
    using std::tr1::shared_ptr;

    TileSet::TileSet() {}
    TileSet::~TileSet() {}

    bool
    TileSet::add(shared_ptr<DocumentTile>& tile)
    {
        string uri = tile->get_document_uri_raw();
        if (tiles_.find(uri) != tiles_.end()) return false;

        tiles_[uri] = tile;
        return true;
    }

    shared_ptr<DocumentTile>
    TileSet::get_tile(const std::string& uri)
    {
        DocumentTile* tile_null = 0;
        shared_ptr<DocumentTile> p(tile_null);

        tile_map::iterator it = tiles_.find(uri);
        if (it != tiles_.end())
            p = it->second;

        return p;
    }

} // namespace paperbox
