/*
 *  Paperbox - tracker-client.cc
 *
 *  Copyright (C) 2007-2008 Marko Anastasov
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include <sys/time.h>
#include <iostream>
#include <vector>
#include <glibmm/convert.h>
#include <glibmm/fileutils.h>
#include <glibmm-utils/log-stream-utils.h>
#include "file-utils.hh"
#include "tag-request.hh"
#include "tracker-phone.hh"
#include "tracker-phone-signals-marshal.h"

namespace {

    static const int DOC_RETRIEVAL_LIMIT = 100000;

    static const char* doc_types[] =
    {
        "application/x-abiword",
        "application/pdf",
        "application/postscript",
        "application/x-gzpostscript",
        "application/vnd.oasis.opendocument.text",
        "application/vnd.oasis.opendocument.text-template",
        "application/vnd.oasis.opendocument.text-web",
        "application/vnd.oasis.opendocument.text-master",
        "application/vnd.oasis.opendocument.presentation",
        "application/vnd.oasis.opendocument.presentation-template",
        "application/vnd.oasis.opendocument.spreadsheet",
        "application/vnd.oasis.opendocument.spreadsheet-template",
        "application/vnd.oasis.opendocument.chart",
        "application/vnd.oasis.opendocument.formula",
        "application/msword",
        "application/vnd.ms-word",
        "application/vnd.ms-excel",
        "application/vnd.ms-powerpoint",
	"text/plain",
        NULL
    };

    static const char* doc_keys[] =
    {
	"File:Name",
	"Doc:Title",
	"Doc:Author",
	"Doc:Comments",
	"Doc:PageCount",
	"Doc:WordCount",
	"Doc:Created",
	NULL
    };

    enum {        // keep in sync with doc_keys
        FILE_NAME,
        TITLE,
        AUTHOR,
        COMMENTS,
        PAGE_COUNT,
        WORD_COUNT,
        DATE_CREATED
    };

    static inline bool
    is_empty_string(const char *s)
    {
	return s == 0 || s[0] == '\0';
    }

    static char**
    ustring_vector_to_array(const std::vector<Glib::ustring>& vec)
    {
        char** array = g_new (char*, vec.size() + 1);
        std::vector<Glib::ustring>::const_iterator it(vec.begin());
        std::vector<Glib::ustring>::const_iterator end(vec.end());

        for (int i = 0; it != end; ++it) {
            array[i++] = g_strdup(it->c_str());
        }

        array[vec.size()] = 0;

        return array;
    }

    void
    ustring_array_to_vector(char** carray, std::vector<Glib::ustring>& vec)
    {
        char** citer;
        for (citer = carray; *citer; ++citer) {
            Glib::ustring ustr(*citer);
            vec.push_back(ustr);
        }
    }

} // anonymous namespace

namespace paperbox {

    struct AsyncTagPack
    {
        TrackerPhone* phone;
        Glib::ustring uri;
    };

} // namespace paperbox

namespace {

    void
    on_tracker_documents_reply(char** results,
                               GError* error,
                               paperbox::TrackerPhone* phone)
    {
        //LOG_DD("got a list of documents by mime type");

        if (error) {
            LOG_ERROR("Ha! No documents: " << error->message);
            g_clear_error(&error);
            return;
        }

        if (! results) {
            LOG_DD("Tracker returned zero documents");
            return;
        }

        char** res_p = 0;
        std::queue<Glib::ustring> uris;

        //LOG_DD("iterating over results");

        for (res_p = results; *res_p; res_p++) {
            Glib::ustring found_uri(*res_p);
            uris.push(found_uri);
        }

        phone->signal_documents().emit(uris);

        g_strfreev(results);
    }

    void
    on_tracker_void_reply(GError* error, paperbox::AsyncTagPack* pack)
    {
        if (! error) return;

        // this is the material for potential signalling which would
        // propagate to some error dialog

        LOG_ERROR("Tracker tag(s) request failed for URI "
                  << pack->uri
                  << ": " << error->message);

        g_clear_error(&error);
        delete pack;
    }

    // Handler for D-Bus signal Tracker.Keywords/KeywordAdded
    void
    on_tracker_keyword_added(DBusGProxy* /*proxy*/,
                             const char* /*service*/,
                             const char* curi,
                             const char* ckeyword,
                             paperbox::TrackerPhone* phone)
    {
        Glib::ustring uri(curi);
        std::vector<Glib::ustring> tags;
        tags.push_back(Glib::ustring(ckeyword));

        phone->signal_tags_added().emit(uri, tags);
    }

    // Handler for D-Bus signal Tracker.Keywords/KeywordRemoved
    void
    on_tracker_keyword_removed(DBusGProxy* /*proxy*/,
                               const char* /*service*/,
                               const char* curi,
                               const char* ckeyword,
                               paperbox::TrackerPhone* phone)
    {
        Glib::ustring uri(curi);
        std::vector<Glib::ustring> tags;
        tags.push_back(Glib::ustring(ckeyword));

        phone->signal_tags_removed().emit(uri, tags);
    }

} // anonymous namespace

namespace paperbox {

    using std::vector;
    using std::tr1::shared_ptr;

    TrackerPhone::TrackerPhone() throw (std::runtime_error)
        :
        tracker_connection_(0),
        bus_(0),
        remote_kw_object_(0)
    {
        gboolean enable_warnings = TRUE;
        tracker_connection_ = tracker_connect(enable_warnings);

        if (! tracker_connection_)
            throw std::runtime_error("Unable to connect to Tracker daemon.");

        connect_to_dbus_signals();
    }

    TrackerPhone::~TrackerPhone()
    {
        tracker_disconnect(tracker_connection_);
    }

    bool
    TrackerPhone::ping_tracker()
    {
        TrackerClient* tc = 0;
        gboolean enable_warnings = TRUE;
        tc = tracker_connect(enable_warnings);
        bool result = (tc);
        tracker_disconnect(tc);
        return result;
    }

    void
    TrackerPhone::connect_to_dbus_signals()
    {
        GError* error = 0;

        // get a bus and proxy
        bus_ = dbus_g_bus_get (DBUS_BUS_SESSION, &error);
        if (!bus_) throw std::runtime_error("Couldn't connect to session bus.");
  
        remote_kw_object_ =
            dbus_g_proxy_new_for_name (bus_,
                                       "org.freedesktop.Tracker",
                                       "/org/freedesktop/tracker",
                                       "org.freedesktop.Tracker.Keywords");
        if (! remote_kw_object_)
            throw std::runtime_error("Failed to get a Tracker.Keywords proxy.");

        // register a marshaller
        dbus_g_object_register_marshaller(
            TRACKER_PHONE__VOID__STRING_STRING_STRING,
            G_TYPE_NONE,
            G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_INVALID);

        // tell DBus what the type signature is,
        // for each of the signal callbacks
        dbus_g_proxy_add_signal(remote_kw_object_,
                                "KeywordAdded",
                                G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING,
                                G_TYPE_INVALID);

        dbus_g_proxy_add_signal(remote_kw_object_,
                                "KeywordRemoved",
                                G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING,
                                G_TYPE_INVALID);

        // connect to signals
        dbus_g_proxy_connect_signal(remote_kw_object_,
                                    "KeywordAdded",
                                    G_CALLBACK(on_tracker_keyword_added),
                                    this, NULL);

        dbus_g_proxy_connect_signal(remote_kw_object_,
                                    "KeywordRemoved",
                                    G_CALLBACK(on_tracker_keyword_removed),
                                    this, NULL);
    }

    void
    TrackerPhone::request_all_documents()
    {
        int live_query_id = -1;
        int offset = 0;
        int max_hits = DOC_RETRIEVAL_LIMIT;

        //LOG_DD("sending request for document URIs");

        tracker_files_get_by_mime_type_async (tracker_connection_,
                                              live_query_id,
                                              const_cast<char**>(doc_types),
                                              offset,
                                              max_hits,
                                              (TrackerArrayReply)
                                              on_tracker_documents_reply,
                                              this);
    }

    /// If you pass an initialized document instance, it will be used
    /// instead of instantiating a new object.
    void
    TrackerPhone::get_document(const Glib::ustring& uri,
                               shared_ptr<Document>& document)
    {
        //LOG_FUNCTION_SCOPE_NORMAL_DD;

        char** results = 0;
        GError* error = 0;

        if (! document) {
            Document* null_doc = 0;
            document.reset(null_doc);
        }

        if (! Glib::file_test(uri.raw(), Glib::FILE_TEST_EXISTS)) {
            throw Glib::FileError(Glib::FileError::NO_SUCH_ENTITY,
                                  "document does not exist");
        }

        //LOG_DD("sending request for metadata for " << uri);

        results = tracker_metadata_get(tracker_connection_,
                                       SERVICE_DOCUMENTS,
                                       uri.c_str(),
                                       doc_keys,
                                       &error);

        if (error) {
            LOG_ERROR("No metadata for " << uri << ": " << error->message);
            g_clear_error(&error);
            return;
        }

        if (! results) {
            LOG_DD("Tracker returned no metadata.");
            return;
        }

        prepare_document(uri, results, document);

        attach_tags(document);

        g_strfreev (results);
    }

    void
    TrackerPhone::prepare_document(const Glib::ustring& uri,
                                   char** keys,
                                   shared_ptr<Document>& document)
    {
        //LOG_FUNCTION_SCOPE_NORMAL_DD;

        //LOG_DD("preparing a document object with metadata for uri " << uri);

        if (! document) document.reset(new Document(uri));

        if (! is_empty_string(keys[FILE_NAME])) {
            Glib::ustring file_name(keys[FILE_NAME]);
            document->set_file_name(file_name);
        }

        guint64 modtime;
        Glib::ustring file_modified;
        get_file_modification_time (uri, modtime, file_modified);
        document->set_modification_time(file_modified);
        document->set_mtime(modtime);

        if (! is_empty_string(keys[TITLE])) {
            Glib::ustring subject(keys[TITLE]);
            // avoid some ugly special cases
            if (subject.lowercase() != "title" &&
                    subject.lowercase() != "untitled") {
                document->set_subject(subject);
            }
        }

        if (! is_empty_string(keys[AUTHOR])) {
            Glib::ustring author(keys[AUTHOR]);
            document->set_author(author);
        }

        if (! is_empty_string(keys[COMMENTS])) {
            Glib::ustring comments(keys[COMMENTS]);
            document->set_comments(comments);
        }

        if (! is_empty_string(keys[PAGE_COUNT])) {
            Glib::ustring page_count(keys[PAGE_COUNT]);
            document->set_page_count(page_count);
        }

        if (! is_empty_string(keys[WORD_COUNT])) {
            Glib::ustring word_count(keys[WORD_COUNT]);
            document->set_word_count(word_count);
        }

        if (! is_empty_string(keys[DATE_CREATED])) {
            Glib::ustring date_created(keys[DATE_CREATED]);
            document->set_date_created(date_created);
        }
    }

    // document information retrieval helper
    void
    TrackerPhone::attach_tags(shared_ptr<Document>& document)
    {
        vector<Glib::ustring> tags;
        TagRequest request(tracker_connection_, document->get_uri());

        request.get_results(tags);

        if (! tags.empty())
            document->set_tags(tags);
    }

    void
    TrackerPhone::add_tags(const Glib::ustring& uri,
                           const vector<Glib::ustring> tags)
    {
        char** ctags = ustring_vector_to_array(tags);
        AsyncTagPack* data = new AsyncTagPack;

        data->phone = this;
        data->uri = uri;

        tracker_keywords_add_async (tracker_connection_,
                                    SERVICE_DOCUMENTS,
                                    uri.c_str(),
                                    ctags,
                                    (TrackerVoidReply) on_tracker_void_reply,
                                    data);
    }

    void
    TrackerPhone::remove_tags(const Glib::ustring& uri,
                              const std::vector<Glib::ustring> tags)
    {
        char** ctags = ustring_vector_to_array(tags);
        AsyncTagPack* data = new AsyncTagPack;

        data->phone = this;
        data->uri = uri;

        tracker_keywords_remove_async (tracker_connection_,
                                       SERVICE_DOCUMENTS,
                                       uri.c_str(),
                                       ctags,
                                       (TrackerVoidReply) on_tracker_void_reply,
                                       data);
    }

} // namespace paperbox
